from django.shortcuts import render
from django.core.context_processors import csrf
from api.views import perform_authenticate_login
from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.contrib import auth
from sendmail.tasks import mailsend
# Create your views here.


#Login For Common
def login_App(request):
    c={}
    c.update(csrf(request))
    if request.method == 'POST':
        username = request.POST.get('username')
        userPwd = request.POST.get('password')
        authenticated, message = perform_authenticate_login(request,username, userPwd) #Check Username and password
        if not authenticated:
            return render(request, 'Login.html', {'error': 'Invalid Credentials'})
        else:
            mailsend.delay(username)
            return HttpResponseRedirect('/dashboard/')
    else:
        return render_to_response('Login.html',c)
#Logout
def logout(request):
    auth.logout(request)
    return HttpResponseRedirect("/login/")


