from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin

from api.views import call_connect, call_and_receive_dtmf, call_and_receive_dtmf_webhook
from missed_call.views import view_missedcall_report, export_csv
from referral.views import add_customer


admin.autodiscover()

urlpatterns = patterns('',

    # For APIs
    url(r'^call_connect/(?P<username>.*)/(?P<password>.*)/(?P<from_number>\d{10,11})/(?P<to_number>\d{10,11})/(?P<caller_id>\d{10,11})/$', call_connect),
    url(r'^call_and_receive_dtmf/(?P<username>.*)/(?P<password>.*)/(?P<to_number>\d{10,11})/(?P<caller_id>\d{10,11})/(?P<webhook_url>.*)$', call_and_receive_dtmf),
    url(r'^call_and_receive_dtmf_webhook/', call_and_receive_dtmf_webhook),

    # For SMS Long Number
    url(r'^long_code/smslongnumber/', "long_code.views.sms_long_number_callback"),
    url(r'^long_code/reports/', "long_code.views.view_longcode_report"),
    url(r'^long_code/export_csv/', "long_code.views.export_csv"),

    # For referral message call backs
    url(r'^referral/smslongcode/', 'referral.views.smslongcode_callback'),
    url(r'^referral/reports/$', 'referral.views.view_report'),
    url(r'^referral/upload/', 'referral.views.upload_referral_csv'),
    url(r'^referral/export_csv/', 'referral.views.export_csv'),
    url(r'^add_customer/$', add_customer),
    url(r'^missedcall/reports/$',view_missedcall_report),
    url(r'^missedcall/export_csv/$',export_csv),

    #For Missedcall
    url(r'^missedcall/sendmail/$','missed_call.views.sendmail'),

    #For Login Application
    url(r'^login/$', 'login.views.login_App'),
    url(r'^logout','login.views.logout'),

    #Dashboard
    url(r'^dashboard/$','dashboard.views.user_Permissions'),
    url(r'^FAQ/$','dashboard.views.faq'),
    url(r'^mapview/$','dashboard.views.map_view'),

    #Calllist_App
    url('^calllist/reports/$','calllist_app.views.calllist_reports'),
    url('^calllist/export_csv/$','calllist_app.views.export_csv'),

    # Admin
    url(r'^admin/', include(admin.site.urls)),

    # Auth
    ('^accounts/', include('django.contrib.auth.urls')),

)

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}))
