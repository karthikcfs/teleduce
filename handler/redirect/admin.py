from django.contrib import admin

from util import common
from util.admin import AuditAdminCallback, AuditAdminCallbackWithNoLinks

from redirect.models import OutboundNumbersConfig, InProgessCounter, CallDuration

class InProgessCounterAdmin(AuditAdminCallback):
    list_display = ('from_extension', 'to_extension', 'from_number', 'from_channel', 'sequence', ) + AuditAdminCallback.list_display

class OutboundNumbersConfigAdmin(AuditAdminCallback):
    list_display = ('from_extension', 'to_extension', 'to_number', 'sequence', 'context', 'in_use') + AuditAdminCallback.list_display
    list_filter  = ('to_extension', 'to_extension')

class CallDurationAdmin(AuditAdminCallbackWithNoLinks):
    change_list_template = "change_list_audio.html"

    def queryset(self, request):
        if request.user.is_superuser:
            self.list_display = ('customer', 'service_type', 'from_extension', 'from_number', 'from_channel', 'unique_id1', 'unique_id2', 'start_time', 'end_time', 'seconds', 'minutes', 'call_type', 'audio_file_player',)
            self.exclude = ()
        elif common.isClientUser(request.user):
            self.list_display = ('from_number', 'start_time', 'end_time', 'call_type', 'seconds', 'audio_file_player',)
            self.readonly_fields = self.list_display
            self.exclude = ('customer', 'service_type', 'from_extension', 'from_channel', 'unique_id1', 'unique_id2' )
        else:
            raise Exception()
        return common.get_user_group_based_queryset_for_call_duration(self, request)

admin.site.register(InProgessCounter, InProgessCounterAdmin)
admin.site.register(OutboundNumbersConfig, OutboundNumbersConfigAdmin)
admin.site.register(CallDuration, CallDurationAdmin)