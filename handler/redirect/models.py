from django.conf import settings
from django.db import models

from customer.models import Customer
from util.constants import SERVICE_TYPE_CHOICES
from util.models import AuditCallback


class InProgessCounter(AuditCallback):
    from_extension      =   models.CharField(max_length = 15)
    to_extension        =   models.CharField(max_length = 15)
    from_number         =   models.CharField(max_length = 15)
    from_channel        =   models.CharField(max_length = 50)
    sequence            =   models.PositiveIntegerField()
    in_use              =   models.BooleanField(default = True)

    def __unicode__(self):
        return  ('[%s]%s') % (self.sequence, self.to_extension)


class OutboundNumbersConfig(AuditCallback):
    from_extension      =   models.CharField(max_length = 15)
    to_extension        =   models.CharField(max_length = 15)
    to_number           =   models.CharField(max_length = 15)
    sequence            =   models.PositiveIntegerField()
    context             =   models.CharField(max_length = 50)
    in_use              =   models.BooleanField(default = False)

    def __unicode__(self):
        return  ('[%s]%s') % (self.sequence, self.to_extension)

class CallDuration(AuditCallback):
    customer            =   models.ForeignKey(Customer, related_name='customer_%(class)s_redirect_set', null = True, blank = True)
    service_type        =   models.CharField(max_length=100, choices = SERVICE_TYPE_CHOICES)
    from_extension      =   models.CharField(max_length = 15)
    from_number         =   models.CharField(max_length = 15)
    from_channel        =   models.CharField(max_length = 50)
    unique_id1          =   models.CharField(max_length = 50)
    unique_id2          =   models.CharField(max_length = 50, null = True, blank = True)
    start_time          =   models.DateTimeField(auto_now_add = True)
    end_time            =   models.DateTimeField(null = True, blank = True)
    call_type           =   models.CharField(max_length = 30, null = True, blank = True)

    def __unicode__(self):
        return  ('%s') % (self.from_number)

    def seconds(self):
        try:
            return int((self.end_time - self.start_time).total_seconds())
        except:
            return "-"

    def minutes(self):
        try:
            seconds = int((self.end_time - self.start_time).total_seconds())
            minutes = seconds / 60.0
            if (minutes - int(minutes)) > 0:
                return int(minutes) + 1
            return int(minutes)
        except:
            return "-"

    def audio_file_player(self):
        """audio player tag for admin"""
        player_string = ""
        try:
            file_url = settings.MEDIA_URL + self.unique_id2.split(".")[0] + "-" + self.from_channel.replace("/", "-") + ".wav"
            player_string = '<ul class="playlist"><li style="width:250px;">\
            <a href="%s">%s</a></li></ul>' % (file_url, self.from_number)
            return player_string
        except:
            pass
    audio_file_player.allow_tags = True
    audio_file_player.short_description = 'Recording'
