from datetime import datetime

from listener.base import AsteriskListener

from redirect.models import CallDuration, InProgessCounter, \
    OutboundNumbersConfig
from util.constants import SERVICE_TYPE_CALL_LOCAL
from customer.models import CustomerNumber


def get_customer_for_from_extension(from_extension):
    try:
        return CustomerNumber.objects.get(number = from_extension).customer
    except:
        return False

class RedirectListener(AsteriskListener):
    def record(self, manager, channel):
        """Hangup the specified channel"""

        cdict = {'Action':'Monitor'}
        cdict['Channel'] = channel
        cdict['Mix']     = 1
        response = manager.send_action(cdict)

        return response


    def set_var(self, manager, channel, variable, value):
        cdict = {'Action':'Setvar'}
        cdict['Channel']    = channel
        cdict['Variable']   = variable
        cdict['value']      = value
        return  manager.send_action(cdict)

    def set_outbound_number(self, manager, channel, to_number):
        return self.set_var(manager, channel, "OUTBOUNDNUMBER", to_number)

    def __init__(self, event, manager):
        if event.name == 'Bridge' and event.headers['Bridgestate'] == 'Link':
            from_number     = event.headers['CallerID1']
            from_channel    = event.headers['Channel1']
            unique_id1      = event.headers['Uniqueid1']
            # update Uniqueid2 to determine the recording file name (unique2 + channel 1 name)
            call_durations = CallDuration.objects.filter(from_number     =   from_number,
                                                         from_channel    =   from_channel,
                                                         unique_id1      =   unique_id1)

            if len(call_durations) > 0:
                call_duration = call_durations[0]
                call_duration.unique_id2 = event.headers['Uniqueid2']
                call_duration.save()

    #         print "=========================================== \n recording"
    #         print record(manager, event.headers['Channel1'])
    #         print record(manager, event.headers['Channel2'])

        if event.name == 'Dial' and event.headers['SubEvent'] == "End":
            from_channel    =   event.headers['Channel']
            unique_id1      =   event.headers['UniqueID']

            call_durations = CallDuration.objects.filter(from_channel    =   from_channel,
                                                         unique_id1       =   unique_id1)
            if len(call_durations) > 0:
                call_duration = call_durations[0]
                call_duration.call_type = event.headers['DialStatus']
                call_duration.save()

        if event.name == 'Hangup':
            from_number     = event.headers['CallerIDNum']
            from_channel    = event.headers['Channel']
            unique_id1      = event.headers['Uniqueid']

            ############# For Billing Start #################
            call_durations = CallDuration.objects.filter(from_number     =   from_number,
                                                         from_channel    =   from_channel,
                                                         unique_id1      =   unique_id1)
            if len(call_durations) > 0:
                call_duration = call_durations[0]
                call_duration.end_time = datetime.now()
                call_duration.save()


            ############# For Billing End #################
            in_progress_counters = InProgessCounter.objects.filter(from_number  =   from_number,
                                                                  from_channel  =   from_channel,
                                                                  in_use        =   True).order_by('-created_at')
            if len(in_progress_counters) > 0:
                in_progress_counter = in_progress_counters[0]
                in_progress_counter.in_use = False
                in_progress_counter.save()

                outbound_number = OutboundNumbersConfig.objects.get(from_extension  =   in_progress_counter.from_extension,
                                                                    to_extension    =   in_progress_counter.to_extension,
                                                                    sequence        =   in_progress_counter.sequence
                                                                    )
                outbound_number.in_use = False
                outbound_number.save()

        if event.name == 'Newchannel'and event.headers['ChannelStateDesc'] == "Ring":
            from_extension  =   event.headers['Exten']
            from_number     =   event.headers['CallerIDNum']
            from_channel    =   event.headers['Channel']
            unique_id1      =   event.headers['Uniqueid']

            customer = get_customer_for_from_extension(from_extension)

            if not customer:
                return

            CallDuration.objects.create(customer        =   customer,
                                        service_type    =   SERVICE_TYPE_CALL_LOCAL,
                                        from_extension  =   from_extension,
                                        from_number     =   from_number,
                                        from_channel    =   from_channel,
                                        unique_id1      =   unique_id1)

        if event.name == 'Newexten'and event.headers['Application'] == "WaitExten":
            from_extension  =   event.headers['Extension']
            from_channel    =   event.headers['Channel']
            from_number     =   event.headers['Channel'].split('/')[2].split('-')[0]

            outbound_numbers = OutboundNumbersConfig.objects.filter(from_extension = from_extension, in_use = False).order_by('sequence')
            if len(outbound_numbers) == 0:
                print "No matches found...."
            for outbound_number in outbound_numbers:
                outbound_number.in_use = True
                outbound_number.save()
                to_extension    =   outbound_number.to_extension
                sequence        =   outbound_number.sequence

                InProgessCounter.objects.create(from_extension  =   from_extension,
                                                from_number     =   from_number,
                                                    to_extension    =   to_extension,
                                                    from_channel    =   from_channel,
                                                    sequence        =   sequence
                                                    )
                print "set var...."
                print self.set_outbound_number(manager, from_channel, outbound_number.to_number)
                print "redirecting...."
                print manager.redirect(from_channel, to_extension, priority='1', extra_channel='', context=outbound_number.context)
                break
