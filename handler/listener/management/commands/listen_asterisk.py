import sys, time

import asterisk.manager
from django.core.management.base import BaseCommand

from handler.settings import ASTERISK_MANAGER_IP_ADDRESS, \
    ASTERISK_MANAGER_USERNAME, ASTERISK_MANAGER_PASSWORD
from listener.models import EventLog, Varset, Rename, Masquerade, Call, Bridge, \
    Channel

# DON'T Remove these lines. For every call back module we need import these two
############################################
import redirect
from redirect.sub import AsteriskListener
import api
from api.sub import AsteriskListener
import missed_call
from missed_call.sub import AsteriskListener
############################################

class Command(BaseCommand):
    help = "Listen to Asterisk Events."

    def handle(self, *args, **options):
        connected = False
        manager = None
        while True:
            if connected:
                time.sleep(5)
                try:
                    manager_msg = manager.ping()
                    if not 'Success' in manager_msg.response[0]:
                        connected = False
                        print "Not Connected."
                except Exception, reason:
                    print "Error: %s" % reason
                    connected = False

            else: # Not connected
                try:
                    manager = asterisk.manager.Manager()
                    print "Attempting to connect " + ASTERISK_MANAGER_IP_ADDRESS
                    manager.connect(ASTERISK_MANAGER_IP_ADDRESS)
                    manager.register_event('*', receive_event)  # catch all
                    manager.login(ASTERISK_MANAGER_USERNAME, ASTERISK_MANAGER_PASSWORD)
                    print "connected to " + ASTERISK_MANAGER_IP_ADDRESS
                    connected = True
                    # manager.logoff()
                except asterisk.manager.ManagerSocketException, (errno, reason):
                    print "Error connecting to the manager: %s - %s" % (errno, reason)
                    connected = False
                except asterisk.manager.ManagerAuthException, reason:
                    print "Error logging in to the manager: %s" % reason
                    connected = False
                except asterisk.manager.ManagerException, reason:
                    print "Error: %s" % reason
                    connected = False
                except Exception, reason:
                    print "Error: %s" % reason
                    connected = False
                finally:
                    pass
                    #manager.close()

def receive_event(event, manager):
    parameter_dict = event.headers
    EventLog.objects.create(event_name=event.name, parameters=str(parameter_dict))
    handle_event(event)

    print "============== " + event.name + " ===================="
    printplus(event.headers)

    for listener in AsteriskListener.__subclasses__():  # @UndefinedVariable
        class_name =  str(listener).split("'")[1]
        print "Dispatching the event to " + class_name
        klass = eval(class_name)
        klass(event, manager)

def handle_event(event):
    if event.name == 'Bridge':
        handle_bridge(event.headers)
    elif event.name == 'Dial':
        handle_dial(event.headers)
    elif event.name == 'Hangup':
        handle_hangup(event.headers)
    elif event.name == 'HangupRequest':
        handle_hangup_request(event.headers)
    elif event.name == 'Masquerade':
        handle_masquerade(event.headers)
    elif event.name == 'NewAccountCode':
        handle_new_account_code(event.headers)
    elif event.name == 'NewCallerid':
        handle_new_caller_id(event.headers)
    elif event.name == 'Newchannel':
        handle_new_channel(event.headers)
    elif event.name == 'Newexten':
        handle_new_extenstion(event.headers)
    elif event.name == 'Newstate':
        handle_new_state(event.headers)
    elif event.name == 'Rename':
        handle_rename(event.headers)
    elif event.name == 'SoftHangupRequest':
        handle_soft_hangup_request(event.headers)
    elif event.name == 'VarSet':
        handle_varset(event.headers)

# = headers[''],
def handle_varset(headers):
    Varset.objects.create(
                               channel=headers['Channel'],
                               unique_id=headers['Uniqueid'],
                               value=headers['Value'],
                               variable=headers['Variable'],
                          )

def handle_soft_hangup_request(headers):
    persist_channel(headers)

def handle_rename(headers):
    Rename.objects.create(
                                channel=headers['Channel'],
                                unique_id=headers['Uniqueid'],
                                new_name=headers['Newname']
                          )

def handle_new_state(headers):
    persist_channel(headers)

def handle_new_extenstion(headers):
    persist_channel(headers)

def handle_new_channel(headers):
    persist_channel(headers)

def handle_new_caller_id(headers):
    persist_channel(headers)

def handle_new_account_code(headers):
    persist_channel(headers)

def handle_masquerade(headers):
    Masquerade.objects.create(
                                clone=headers['Clone'],
                                clone_state=headers['CloneState'],
                                event=headers['Event'],
                                original=headers['Original'],
                                original_state=headers['OriginalState']
                            )

def handle_hangup_request(headers):
    persist_channel(headers)

def handle_hangup(headers):
    persist_channel(headers)

def handle_dial(headers):
    Call.objects.create(
                            event=headers['Event']              if 'Event'              in headers else '',
                            sub_event=headers['SubEvent']           if 'SubEvent'           in headers else '',
                            channel=headers['Channel']            if 'Channel'            in headers else '',
                            unique_id=headers['UniqueID']           if 'UniqueID'           in headers else '',
                            dial_status=headers['DialStatus']         if 'DialStatus'         in headers else '',
                            dial_string=headers['Dialstring']         if 'Dialstring'         in headers else '',
                            destination=headers['Destination']        if 'Destination'        in headers else '',
                            dest_unique_id=headers['DestUniqueID']       if 'DestUniqueID'       in headers else '',
                            caller_id_name=headers['CallerIDName']       if 'CallerIDName'       in headers else '',
                            caller_id_num=headers['CallerIDNum']        if 'CallerIDNum'        in headers else '',
                            connected_line_name=headers['ConnectedLineName']  if 'ConnectedLineName'  in headers else '',
                            connected_line_num=headers['ConnectedLineNum']   if 'ConnectedLineNum'   in headers else ''
                        )


def handle_bridge(headers):
    Bridge.objects.create(
                            bridge_state=headers['Bridgestate'],
                            bridge_type=headers['Bridgetype'],
                            caller_id1=headers['CallerID1'],
                            caller_id2=headers['CallerID2'],
                            channel1=headers['Channel1'],
                            channel2=headers['Channel2'],
                            event=headers['Event'],
                            unique_id1=headers['Uniqueid1'],
                            unique_id2=headers['Uniqueid2']
                          )

def persist_channel(headers):
#    print "=" * 1000
#    print headers['ChannelStateDesc']  if 'ChannelStateDesc'   in headers else '',
#    print "=" * 1000
    extension = headers['Extension']  if 'Extension' in headers else None
    if not extension:
        extension = headers['Exten'] if 'Exten' in headers else ''

    Channel.objects.create(
                            event_name=headers['Event']                     if 'Event'              in headers else '',
                            channel=headers['Channel']                   if 'Channel'            in headers else '',
                            unique_id=headers['Uniqueid']                  if 'Uniqueid'           in headers else '',
                            account_code=headers['AccountCode']               if 'AccountCode'        in headers else '',
                            old_account_code=headers['OldAccountCode']        if 'OldAccountCode'     in headers else '',
                            caller_id_name=headers['CallerIDName']          if 'CallerIDName'       in headers else '',
                            caller_id_num=headers['CallerIDNum']           if 'CallerIDNum'        in headers else '',
                            channel_state=headers['ChannelState']      if 'ChannelState'       in headers else '',
                            channel_state_desc=headers['ChannelStateDesc']  if 'ChannelStateDesc'   in headers else '',
                            connected_line_num=headers['ConnectedLineNum']  if 'ConnectedLineNum'   in headers else '',
                            connected_line_name=headers['ConnectedLineName'] if 'ConnectedLineName'  in headers else '',
                            context=headers['Context']                   if 'Context'            in headers else '',
                            extension=extension,
                            application=headers['Application']               if 'Application'        in headers else '',
                            app_data=headers['AppData']                   if 'AppData'            in headers else '',
                            priority=headers['Priority']                  if 'Priority'           in headers else '',
                            cid_calling_pres=headers['CID-CallingPres']       if 'CID-CallingPres'    in headers else '',
                            cause=headers['Cause']                     if 'Cause'              in headers else '',
                            cause_txt=headers['Cause-txt']                 if 'Cause-txt'          in headers else ''
                        )



def printplus(obj):
    """
    Pretty-prints the object passed in.

    """
    # Dict
    if isinstance(obj, dict):
        for k, v in sorted(obj.items()):
            print u'{0}: {1}'.format(k, v)

    # List or tuple
    elif isinstance(obj, list) or isinstance(obj, tuple):
        for x in obj:
            print x
    # Other
    else:
        print obj

