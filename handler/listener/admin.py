
from django.contrib import admin

from util.admin import AuditAdminCallback

from .models import Channel, Masquerade, Rename, Call, Bridge, Varset, EventLog


class ChannelAdmin(AuditAdminCallback):
    list_display = ('channel', 'event_name', 'extension', 'context', 'channel_state_desc', 'cause_txt',) + AuditAdminCallback.list_display

class MasqueradeAdmin(AuditAdminCallback):
    list_display = ('clone', 'clone_state', 'event', 'original', 'original_state',)  + AuditAdminCallback.list_display

class RenameAdmin(AuditAdminCallback):
    list_display = ('channel', 'new_name',)  + AuditAdminCallback.list_display

class CallAdmin(AuditAdminCallback):
    list_display = ('event', 'sub_event', 'channel', 'destination', 'dial_status', 'caller_id_name', 'caller_id_num', 'connected_line_name', 'connected_line_num', ) + AuditAdminCallback.list_display

class BridgeAdmin(AuditAdminCallback):
    list_display = ('bridge_state', 'bridge_type', 'caller_id1', 'caller_id2', 'channel1', 'channel2', 'event',) + AuditAdminCallback.list_display

class VarsetAdmin(AuditAdminCallback):
    list_display = ('channel', 'variable', 'value',)  + AuditAdminCallback.list_display

class EventLogAdmin(AuditAdminCallback):
    list_display = ('event_name', 'parameters', ) + AuditAdminCallback.list_display
    list_filter  = ('event_name', )

admin.site.register(Channel, ChannelAdmin)
admin.site.register(Masquerade, MasqueradeAdmin)
admin.site.register(Rename, RenameAdmin)
admin.site.register(Call, CallAdmin)
admin.site.register(Bridge, BridgeAdmin)
admin.site.register(Varset, VarsetAdmin)
admin.site.register(EventLog, EventLogAdmin)
