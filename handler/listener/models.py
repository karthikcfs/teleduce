from django.db import models
from util.models import AuditCallback

class Channel(AuditCallback):
    unique_id           = models.CharField(max_length = 50)
    event_name          = models.CharField(max_length = 50)# choices = MODEL_CHANNEL_EVENTS)
    account_code        = models.CharField(max_length = 50, null = True, blank = True)
    old_account_code    = models.CharField(max_length = 50, null = True, blank = True)
    caller_id_name      = models.CharField(max_length = 50, null = True, blank = True)
    caller_id_num       = models.CharField(max_length = 50, null = True, blank = True)
    channel             = models.CharField(max_length = 50, null = True, blank = True)
    channel_state       = models.CharField(max_length = 10, null = True, blank = True)
    channel_state_desc  = models.CharField(max_length = 50, null = True, blank = True)
    context             = models.CharField(max_length = 50, null = True, blank = True)
    extension           = models.CharField(max_length = 50, null = True, blank = True)
    connected_line_name = models.CharField(max_length = 50, null = True, blank = True)
    connected_line_num  = models.CharField(max_length = 50, null = True, blank = True)
    application         = models.CharField(max_length = 50, null = True, blank = True)
    app_data            = models.CharField(max_length = 50, null = True, blank = True)
    priority            = models.CharField(max_length = 10, null = True, blank = True)
    cid_calling_pres    = models.CharField(max_length = 50, null = True, blank = True)
    cause               = models.CharField(max_length = 50, null = True, blank = True)
    cause_txt           = models.CharField(max_length = 50, null = True, blank = True)

    def __unicode__(self):
        return  ('%s[%s]') % (self.channel, self.event_name)



class Masquerade(AuditCallback):
    clone           = models.CharField(max_length = 50, null = True, blank = True)
    clone_state     = models.CharField(max_length = 50, null = True, blank = True)
    event           = models.CharField(max_length = 50, null = True, blank = True)
    original        = models.CharField(max_length = 50, null = True, blank = True)
    original_state  = models.CharField(max_length = 50, null = True, blank = True)

    def __unicode__(self):
        return  ('%s[%s]') % (self.clone, self.clone_state)

class Rename(AuditCallback):
    unique_id               = models.CharField(max_length = 50, null = True, blank = True)
    channel     = models.CharField(max_length = 50, null = True, blank = True)
    new_name    = models.CharField(max_length = 50, null = True, blank = True)

    def __unicode__(self):
        return  ('%s[%s]') % (self.channel, self.new_name)


class Call(AuditCallback):
    event                   = models.CharField(max_length = 50, null = True, blank = True)
    sub_event               = models.CharField(max_length = 50, null = True, blank = True)
    channel                 = models.CharField(max_length = 50, null = True, blank = True)
    destination             = models.CharField(max_length = 50, null = True, blank = True)
    dial_string             = models.CharField(max_length = 50, null = True, blank = True)
    dial_status             = models.CharField(max_length = 50, null = True, blank = True)
    caller_id_name          = models.CharField(max_length = 50, null = True, blank = True)
    caller_id_num           = models.CharField(max_length = 50, null = True, blank = True)
    connected_line_name     = models.CharField(max_length = 50, null = True, blank = True)
    connected_line_num      = models.CharField(max_length = 50, null = True, blank = True)
    dest_unique_id          = models.CharField(max_length = 50, null = True, blank = True)
    unique_id               = models.CharField(max_length = 50, null = True, blank = True)

    def __unicode__(self):
        return  ('%s[%s]') % (self.event, self.sub_event)


class Bridge(AuditCallback):
    bridge_state    = models.CharField(max_length = 50, null = True, blank = True)
    bridge_type     = models.CharField(max_length = 50, null = True, blank = True)
    caller_id1      = models.CharField(max_length = 50, null = True, blank = True)
    caller_id2      = models.CharField(max_length = 50, null = True, blank = True)
    channel1        = models.CharField(max_length = 50, null = True, blank = True)
    channel2        = models.CharField(max_length = 50, null = True, blank = True)
    event           = models.CharField(max_length = 50, null = True, blank = True)
    unique_id1      = models.CharField(max_length = 50, null = True, blank = True)
    unique_id2      = models.CharField(max_length = 50, null = True, blank = True)

    def __unicode__(self):
        return  ('%s[%s]') % (self.caller_id1, self.caller_id1)



class Varset(AuditCallback):
    unique_id   = models.CharField(max_length = 50, null = True, blank = True)
    channel     = models.CharField(max_length = 50, null = True, blank = True)
    variable    = models.CharField(max_length = 50, null = True, blank = True)
    value       = models.CharField(max_length = 255, null = True, blank = True)

    def __unicode__(self):
        return  ('%s(%s = %s') % (self.channel, self.variable, self.value)

class EventLog(AuditCallback):
    event_name = models.CharField(max_length = 50)
    parameters  = models.TextField()

    def __unicode__(self):
        return  ('%s') % (self.event_name)

