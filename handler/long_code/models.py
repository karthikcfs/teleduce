from django.db import models

from customer.models import Customer
from util.models import Audit, AuditCallback


class SmsLongNumberConfig(Audit):
    customer            = models.ForeignKey(Customer, unique = True)
    # INBOUND
    sms_long_number     = models.CharField(max_length = 15)
    sms_keyword         = models.CharField(max_length = 15)
    # OUTBOUND
    web_hook_url         = models.CharField(max_length = 255, null = True, blank = True)
    sms_sender_id        = models.CharField(max_length = 50, null = True, blank = True)
    sms_account_username = models.CharField(max_length = 50, null = True, blank = True)
    sms_account_password = models.CharField(max_length = 50, null = True, blank = True)
    sms_reply_text       = models.TextField(null = True, blank = True)

    def __unicode__(self):
        return self.customer.name

    class Meta:
        unique_together = ("sms_long_number", 'sms_keyword')

class SmsLongNumberCallback(AuditCallback):
    sms_sid                 =   models.CharField(max_length = 50, verbose_name = 'SMS SID')
    sms_from                =   models.CharField(max_length = 13, verbose_name = 'SMS From')
    sms_to                  =   models.CharField(max_length = 13, verbose_name = 'SMS To')
    received_at             =   models.DateTimeField(verbose_name = 'SMS Received At')
    body                    =   models.CharField(max_length = 255, verbose_name = 'SMS Content')

    def __unicode__(self):
        return self.sms_from

    class Meta:
        verbose_name        = "SMS Long Number -> Callback"
        verbose_name_plural = "SMS Long Number -> Callbacks"

class SmsLongNumberInboundMessage(Audit):
    customer            = models.ForeignKey(Customer)
    sms_long_number     = models.CharField(max_length = 15)
    sms_keyword         = models.CharField(max_length = 15)
    sms_from            = models.CharField(max_length = 13, verbose_name = 'SMS From')
    received_at         = models.DateTimeField(verbose_name = 'SMS Received At')
    body                = models.CharField(max_length = 255, verbose_name = 'SMS Content')

    def __unicode__(self):
        return self.customer.name


class ReplyLog(AuditCallback):
    customer                = models.ForeignKey(Customer)
    sms_http_request        = models.TextField(null = True, blank = True)
    sms_http_response       = models.TextField(null = True, blank = True)
    web_hook_http_request   = models.TextField(null = True, blank = True)
    web_hook_http_response  = models.TextField(null = True, blank = True)

    def __unicode__(self):
        return self.customer.name


class SmsLongNumberCreditMaster(Audit):
    customer        = models.ForeignKey(Customer)
    current_credits = models.IntegerField(default=0)
    from_validity   = models.DateField()
    to_validity     = models.DateField()

    def __unicode__(self):
        return self.customer.name
