import datetime

from long_code.models import SmsLongNumberCreditMaster


def verify_credits(customer):
    credit_masters  = SmsLongNumberCreditMaster.objects.filter(customer = customer)
    if not credit_masters:
        return False, "Credit entries not found."

    credit_master =  credit_masters[0]
    if not credit_master.current_credits > 0:
        return False, "Credits are less. Current Credits = %s" % credit_master.current_credits

    if credit_master.from_validity > datetime.date.today():
        return False, "The credits are valid only from %s" %  credit_master.from_validity

    if credit_master.to_validity < datetime.date.today():
        return False, "Validity expired on  %s" %  credit_master.to_validity

    return True, "Valid"

def deduct_credit(customer):
    credit_master = SmsLongNumberCreditMaster.objects.get(customer = customer)
    credit_master.current_credits = credit_master.current_credits - 1
    credit_master.save()
