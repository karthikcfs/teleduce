import datetime

from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver

from long_code import credit_helper
from long_code.models import SmsLongNumberCallback, SmsLongNumberConfig, \
    SmsLongNumberInboundMessage, ReplyLog
from util import web_hook_util, sms_util


@receiver(post_save, sender = SmsLongNumberCallback)
def determine_customer_and_extract_referral(sender, **kwargs):
    sms_long_number_callback = kwargs['instance']

    sms_long_number     =   sms_long_number_callback.sms_to
    sms_original_body   =   sms_long_number_callback.body
    sms_keyword         =   sms_original_body.split(' ')[0]
    sms_body            =   sms_original_body[len(sms_keyword):] # Just remove the keyword
    sms_received_from   =   sms_long_number_callback.sms_from
    sms_received_at     =   sms_long_number_callback.received_at

    # Determine the customer config
    potential_configs = SmsLongNumberConfig.objects.filter(sms_long_number  = sms_long_number,
                                                           sms_keyword      = sms_keyword)
    if not potential_configs:
        return # Junk request

    matched_config = potential_configs[0]

    SmsLongNumberInboundMessage.objects.create(     customer            = matched_config.customer,
                                                    sms_long_number     = matched_config.sms_long_number,
                                                    sms_keyword         = matched_config.sms_keyword,
                                                    sms_from            = sms_received_from,
                                                    received_at         = sms_received_at,
                                                    body                = sms_body)

    # Reduce one credit for incoming SMS
    credit_helper.deduct_credit(matched_config.customer)

    # Send Web Hook Request
    reply_log = ReplyLog.objects.create(customer = matched_config.customer)

    if matched_config.web_hook_url:
        params = {  'sms_from' : sms_received_from,
                    'body'     : sms_body
                  }
        http_response = web_hook_util.call_web_hook_url(matched_config.web_hook_url, params)
        reply_log.web_hook_http_request  = matched_config.web_hook_url + " -> " + str(params)
        reply_log.web_hook_http_response = http_response
        reply_log.save()

    # Send SMS reply only if all the parameters are configured
    if matched_config.sms_sender_id and \
       matched_config.sms_account_username and \
       matched_config.sms_account_password and \
       matched_config.sms_reply_text:

        valid, _ = credit_helper.verify_credits(matched_config.customer)
        if valid:

            http_response = sms_util.send_sms(matched_config.sms_account_username,
                                              matched_config.sms_account_password,
                                              matched_config.sms_sender_id,
                                              sms_received_from,
                                              matched_config.sms_reply_text)

            reply_log.sms_http_request  = sms_received_from + " -> " + matched_config.sms_reply_text
            reply_log.sms_http_response = http_response
            reply_log.save()

            # Reduce one credit for reply SMS
            credit_helper.deduct_credit(matched_config.customer)
