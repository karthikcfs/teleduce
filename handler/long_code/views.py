from django.http.response import HttpResponse
from long_code.models import SmsLongNumberCallback,SmsLongNumberInboundMessage,SmsLongNumberCreditMaster
from referral.models import ReferralCreditMaster
from util import sms_util, common, credits
from util.common import get_customer_for_client_user
from django.shortcuts import render
from django.http.response import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required, user_passes_test
import csv


def sms_long_number_callback(request):
    if not request.method in ('GET', 'HEAD'):
        response =  HttpResponse()
        response.status_code = 405
        return response

    if request.method == 'HEAD':
        return  HttpResponse(content_type = "text/plain")

    sms_sid                 =   request.GET.get('SmsSid')
    sms_from                =   request.GET.get('From')
    sms_to                  =   request.GET.get('To')
    received_at             =   request.GET.get('Date')
    body                    =   request.GET.get('Body')

    received_at = received_at.replace("'", "")

    SmsLongNumberCallback.objects.create(
                                                sms_sid                 =   sms_sid,
                                                sms_from                =   sms_from,
                                                sms_to                  =   sms_to,
                                                received_at             =   received_at,
                                                body                    =   body
                                          )

    return HttpResponse()
def credits_count(request):
    customer = common.get_customer_for_client_user(request.user)
    if not customer:
        return HttpResponseRedirect('/login')
    Isms_credit_masters  = SmsLongNumberCreditMaster.objects.filter(customer = customer)
    Referral_credit_masters  = ReferralCreditMaster.objects.filter(customer = customer)
    if Isms_credit_masters:
        Isms_credit_masters_list = []
        for customer_credit_info in Isms_credit_masters:
            datas = { 'current_credits'  : customer_credit_info.current_credits,
                      'from_validity'    : customer_credit_info.from_validity,
                      'to_validity'      : customer_credit_info.to_validity,}
            Isms_credit_masters_list.append(datas)
            return Isms_credit_masters_list
    elif Referral_credit_masters:
        credit_masters_list = []
        for referal_credit_info in Referral_credit_masters:
            datas = { 'current_credits'  : referal_credit_info.current_credits,
                      'from_validity'    : referal_credit_info.from_validity,
                      'to_validity'      : referal_credit_info.to_validity,}
            credit_masters_list.append(datas)
            return credit_masters_list

def view_longcode_report(request):
    customer = common.get_customer_for_client_user(request.user)
    user = request.user
    session_id = user.id
    groupnames = user.groups.filter(user=session_id)
    referal_credit_masters_list   = credits.referral_credits_count(user)
    incommingsms_credit_masters_list = credits.incomming_credits_count(user)
    if not customer:
        return HttpResponseRedirect('/login/')
    LongNumber_Infos = SmsLongNumberInboundMessage.objects.filter(customer = customer)
    LongNumber_List = []
    for LongNumber_Info in LongNumber_Infos:
        LongNumber_data = {'sms_id'                          : LongNumber_Info.id,
                           'sms_long_number'                 : LongNumber_Info.sms_long_number,
                           'sms_keyword'                     : LongNumber_Info.sms_keyword,
                           'sms_from'                        : LongNumber_Info.sms_from,
                           'created_at'                      : LongNumber_Info.created_at,
                           'body'                            : LongNumber_Info.body,
                           }
        LongNumber_List.append(LongNumber_data)
    credit_masters  = SmsLongNumberCreditMaster.objects.filter(customer = customer)
    Isms_credit_masters_list = []
    for customer_credit_info in credit_masters:
        datas = { 'current_credits'  : customer_credit_info.current_credits,
                  'from_validity'    : customer_credit_info.from_validity,
                  'to_validity'      : customer_credit_info.to_validity,}
        Isms_credit_masters_list.append(datas)
    return render(request, "LongNumber_List.html", {'LongNumber_List': LongNumber_List ,'customer' : customer, 'groupnames': groupnames,'credit_masters_list' : referal_credit_masters_list, 'isms_credit_masters_list': incommingsms_credit_masters_list })

def export_csv(request):
    customer = common.get_customer_for_client_user(request.user)
    if not customer:
        return HttpResponseRedirect('/login/')
    InboundMessage_Infos = SmsLongNumberInboundMessage.objects.filter(customer = customer)
    response = HttpResponse(mimetype='text/csv')
    response['Content-Disposition'] = 'attachment; filename="InboundMessage_list.csv"'
    writer = csv.writer(response)
    writer.writerow([ 'LongNumber' , 'sms_keyword','From Number' ,'Date', 'SMS Text'])
    for report_data in InboundMessage_Infos:
        writer.writerow([report_data.sms_long_number, report_data.sms_keyword , report_data.sms_from, report_data.created_at, report_data.body])
    return response
