from django.contrib import admin

from long_code.models import SmsLongNumberConfig, SmsLongNumberCallback, \
    SmsLongNumberInboundMessage, ReplyLog, SmsLongNumberCreditMaster
from util.admin import AuditAdmin, AuditAdminCallback


class SmsLongNumberConfigAdmin(AuditAdmin):
    list_display = (
                        "customer",
                        "sms_long_number",
                        "web_hook_url",
                        "sms_keyword",
                        "sms_sender_id",
                        "sms_account_username",
                        "sms_account_password",
                        "sms_reply_text",
                    ) + AuditAdmin.list_display


class SmsLongNumberCallbackAdmin(AuditAdminCallback):
    list_display = (
                        "sms_sid",
                        "sms_from",
                        "sms_to",
                        "received_at",
                        "body",
                    ) + AuditAdminCallback.list_display


class SmsLongNumberInboundMessageAdmin(AuditAdminCallback):
    list_display = (
                        "customer",
                        "sms_long_number",
                        "sms_keyword",
                        "sms_from",
                        "received_at",
                        "body",
                    ) + AuditAdminCallback.list_display


class ReplyLogAdmin(AuditAdminCallback):
    list_display = (
                        "customer",
                        "sms_http_request",
                        "sms_http_response",
                        "web_hook_http_request",
                        "web_hook_http_response",
                    ) + AuditAdminCallback.list_display


class SmsLongNumberCreditMasterAdmin(AuditAdmin):
    list_display = (
                        "customer",
                        "current_credits",
                        "from_validity",
                        "to_validity",
                    ) + AuditAdmin.list_display

admin.site.register(SmsLongNumberConfig, SmsLongNumberConfigAdmin)
admin.site.register(SmsLongNumberCallback, SmsLongNumberCallbackAdmin)
admin.site.register(SmsLongNumberInboundMessage, SmsLongNumberInboundMessageAdmin)
admin.site.register(ReplyLog, ReplyLogAdmin)
admin.site.register(SmsLongNumberCreditMaster, SmsLongNumberCreditMasterAdmin)
