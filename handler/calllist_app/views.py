from django.shortcuts import render
from util import common, credits
import csv
from django.http.response import  HttpResponseRedirect,HttpResponse
from django.core.context_processors import csrf
from api.models import CallDuration



# Create your views here.
#call_list App
def calllist_reports(request):
    c={}
    c.update(csrf(request))
    customer    = common.get_customer_for_client_user(request.user)
    user        = request.user
    session_id  = user.id
    groupnames  = user.groups.filter(user=session_id)
    referal_credit_masters_list      = credits.referral_credits_count(user)  #referral credits count
    incommingsms_credit_masters_list = credits.incomming_credits_count(user) #incommingsms count
    if not customer:
        return HttpResponseRedirect('/login/')
    apicallduration_Lists            = apicallduration_list(request,customer)
    redirectcallduration_Lists       = credits.callduration_list(request,customer)
    return render(request, "calllist_app.html",{'apicallduration_List'     : apicallduration_Lists ,
                                                'redirectcallduration_List': redirectcallduration_Lists ,
                                                'customer'                 : customer,
                                                'groupnames'               : groupnames,
                                                'credit_masters_list'      : referal_credit_masters_list,
                                                'isms_credit_masters_list' : incommingsms_credit_masters_list,})

def apicallduration_list(request,customer):
    apicallduration_Infos = CallDuration.objects.filter(customer = customer)

    apicallduration_Lists = []
    for apicallduration_Info in apicallduration_Infos:
        hang_up_cause     = apicallduration_Info.hang_up_cause

        print hang_up_cause
        if   hang_up_cause == "0":
            calltype       = "Answer"
        elif hang_up_cause == "16":
            calltype       = "Disconnected"
        elif hang_up_cause == "19":
            calltype         = "No Answer"
        elif hang_up_cause == "21":
            calltype       = "Call Rejected"
        elif hang_up_cause == "31":
            calltype       = "Unspecified"
        elif hang_up_cause == "42":
            calltype       = "Congestion"
        else:
            calltype       = "Not Response"

        start_time           = apicallduration_Info.start_time
        end_time             = apicallduration_Info.end_time
        if start_time == None:
            difftimes       = 0
            diff_seconds    = 0
            diff_minutes    = 0
            remainder       = 0
        elif end_time == None:
            difftimes       = 0
            diff_seconds    = 0
            diff_minutes    = 0
            remainder       = 0
        else:
            difftimes            = end_time - start_time
            diff_seconds         = int (difftimes.total_seconds())
            diff_minutes         = diff_seconds/60
            remainder            = diff_seconds % 10
        if remainder == 0:
            diff_minutes = diff_minutes
        else:
            diff_minutes = int(diff_minutes + 1)
        apicallduration_data = {'api_cid'                  : apicallduration_Info.customer_id,
                                'api_customer'             : customer,
                                'api_service_type'         : apicallduration_Info.service_type,
                                'api_from_number'          : apicallduration_Info.from_number,
                                'api_from_channel'         : apicallduration_Info.from_channel,
                                'api_starttime'            : apicallduration_Info.start_time,
                                'api_endtime'              : apicallduration_Info.end_time,
                                'api_seconds'              : diff_seconds,
                                'api_minutes'              : diff_minutes,
                                'api_calltype'             : calltype,

                                }
        apicallduration_Lists.append(apicallduration_data)
    return apicallduration_Lists


def export_csv(request):
    customer = common.get_customer_for_client_user(request.user)
    if not customer:
        return HttpResponseRedirect('/login/')
    apicallduration_Infos       = CallDuration.objects.filter(customer = customer)
    redirectcallduration_Infos  = credits.get_redirct_callduraion_data(request,customer)
    response = HttpResponse(mimetype='text/csv')
    response['Content-Disposition'] = 'attachment; filename="Call_list.csv"'
    writer = csv.writer(response)
    writer.writerow(['From_Number','Start_Time','End_Time','Seconds','Minutes','Call Response'])
    for report_data in apicallduration_Infos:
        hang_up_cause     = report_data.hang_up_cause
        print hang_up_cause
        if   hang_up_cause == "0":
            calltype       = "Answer"
        elif hang_up_cause == "16":
            calltype       = "Disconnected"
        elif hang_up_cause == "19":
            calltype         = "No Answer"
        elif hang_up_cause == "21":
            calltype       = "Call Rejected"
        elif hang_up_cause == "31":
            calltype       = "Unspecified"
        elif hang_up_cause == "42":
            calltype       = "Congestion"
        else:
            calltype       = "Not Response"
        start_time           = report_data.start_time
        end_time             = report_data.end_time
        if start_time == None:
            difftimes       = 0
            diff_seconds    = 0
            diff_minutes    = 0
            remainder       = 0
            report_data.start_time= 0
        elif end_time == None:
            difftimes       = 0
            diff_seconds    = 0
            diff_minutes    = 0
            remainder       = 0
            report_data.end_time = 0
        else:
            difftimes            = end_time - start_time
            diff_seconds         = int (difftimes.total_seconds())
            diff_minutes         = diff_seconds/60
            remainder            = diff_seconds % 10
        if remainder == 0:
            diff_minutes = diff_minutes
        else:
            diff_minutes = int(diff_minutes + 1)
        writer.writerow([report_data.from_number, report_data.start_time, report_data.end_time, diff_seconds,diff_minutes, calltype ])
    for redirectreport_data in redirectcallduration_Infos:
        start_time           = redirectreport_data.start_time
        end_time             = redirectreport_data.end_time
        if start_time == None:
            difftimes       = 0
            diff_seconds    = 0
            diff_minutes    = 0
            remainder       = 0
            redirectreport_data.start_time= 0
        elif end_time == None:
            difftimes       = 0
            diff_seconds    = 0
            diff_minutes    = 0
            remainder       = 0
            redirectreport_data.end_time = 0
        else:
            difftimes            = end_time - start_time
            diff_seconds         = int (difftimes.total_seconds())
            diff_minutes         = diff_seconds/60
            remainder            = diff_seconds % 10
        if(remainder == 0):
            diff_minutes = diff_minutes
        else:
            diff_minutes = int(diff_minutes + 1)
        writer.writerow([redirectreport_data.from_number, redirectreport_data.start_time, redirectreport_data.end_time,diff_seconds,diff_minutes, redirectreport_data.call_type ])
    return response




