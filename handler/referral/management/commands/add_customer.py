from django.core.management.base import BaseCommand

from referral.models import Config, CustomerInfo

class Command(BaseCommand):
    help = "."

    def handle(self, *args, **options):
        customer = Config.objects.all()[0].customer
        CustomerInfo.objects.all().delete()
        s = CustomerInfo.objects.create(customer = customer,
                                    name = "Siva",
                                    mobile_number  = "8884088830")
