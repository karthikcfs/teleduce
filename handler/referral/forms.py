
from django import forms

class ReferralUploadForm(forms.Form):
    file = forms.FileField()
