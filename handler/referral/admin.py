from django.contrib import admin
from django.db.models import get_models, get_app

from referral.models import Config, CustomerInfo, CustomerSmsSentLog, \
    ReferralInfo, SmsLongNumberCallback, ReferralCreditMaster
from util.admin import AuditAdmin, AuditAdminCallbackWithNoLinks


class ConfigAdmin(AuditAdmin):
    list_display = (
                    'customer',
                    'call_customer',
                    'call_referral',
                    'max_retry_customer',
                    'sms_long_number',
                    'sms_sender_id',
                    'sms_keyword',
                    'sms_account_username',
                    'sms_account_password',
                    'sms_text_template_customer',
                    'sms_text_template_referral'
                    ) + AuditAdmin.list_display

class CustomerInfoAdmin(AuditAdminCallbackWithNoLinks): # Customer's customer
    list_display = (
                    'customer',
                    'request_id',
                    'name',
                    'mobile_number'
                    ) + AuditAdminCallbackWithNoLinks.list_display

class CustomerSmsSentLogAdmin(AuditAdminCallbackWithNoLinks):
    list_display = (
                    'customer_info',
                    'sms_text',
                    'sms_delivery_content',
                    ) + AuditAdminCallbackWithNoLinks.list_display

class ReferralInfoAdmin(AuditAdminCallbackWithNoLinks):
    list_display = (
                    'customer_info',
                    'name',
                    'mobile_number',
                    'sms_text',
                    'sms_delivery_content',
                    ) + AuditAdminCallbackWithNoLinks.list_display

class SmsLongNumberCallbackAdmin(AuditAdminCallbackWithNoLinks):
    list_display = (
                    'sms_sid',
                    'sms_from',
                    'sms_to',
                    'received_at',
                    'body'
                    ) + AuditAdminCallbackWithNoLinks.list_display

class ReferralCreditMasterAdmin(AuditAdmin):
    list_display = (
                    'customer',
                    'current_credits',
                    'from_validity',
                    'to_validity'
                    ) + AuditAdmin.list_display

admin.site.register(Config,ConfigAdmin)
admin.site.register(CustomerInfo, CustomerInfoAdmin)
admin.site.register(CustomerSmsSentLog, CustomerSmsSentLogAdmin)
admin.site.register(ReferralInfo, ReferralInfoAdmin)
admin.site.register(SmsLongNumberCallback, SmsLongNumberCallbackAdmin)
admin.site.register(ReferralCreditMaster, ReferralCreditMasterAdmin)
