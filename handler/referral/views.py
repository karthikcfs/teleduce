import csv
import datetime

from django.contrib import messages
from django.core.context_processors import csrf
from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver
from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from referral.forms import ReferralUploadForm
from referral.models import ReferralCreditMaster, ReferralInfo
from referral.models import SmsLongNumberCallback, CustomerInfo, Config, \
    CustomerSmsSentLog
from util import sms_util, common, credits


def upload_referral_csv(request):
    c={}
    c.update(csrf(request))
    customer = common.get_customer_for_client_user(request.user)
    existnumber_count = 0
    user = request.user
    session_id = user.id
    groupnames = user.groups.filter(user=session_id)
    referal_credit_masters_list   = credits.referral_credits_count(user)
    incommingsms_credit_masters_list = credits.incomming_credits_count(user)
    if not customer:
        return HttpResponseRedirect('/login/')
    is_valid, message = verify_credits(customer)
    credits_error = message
#     validity_msg = messages.add_message(request, messages.INFO, message )
    if not is_valid:
        form = ReferralUploadForm()
        return render(request, 'referral_csv_upload.html', {'form': form,'customer' : customer,'credits_error': credits_error, 'groupnames': groupnames, 'credit_masters_list' : referal_credit_masters_list, 'isms_credit_masters_list': incommingsms_credit_masters_list})
    if request.method == 'POST':
        form = ReferralUploadForm(request.POST, request.FILES)
        if form.is_valid():
            csv_file = request.FILES['file']
            customer_records = [row for row in csv.reader(csv_file.read().splitlines())]
            credit_master = ReferralCreditMaster.objects.get(customer = customer)
            current_credits = credit_master.current_credits
            if current_credits < len(customer_records):
                message = "Uploaded file contains %s records. But only %s credits available" % (len(customer_records), current_credits)
                return render(request, 'referral_csv_upload.html', {'form': form, 'message': message, 'customer' : customer, 'credit_masters_list' : referal_credit_masters_list, 'isms_credit_masters_list': incommingsms_credit_masters_list})
            for customer_record in customer_records:
                Mobile_number = customer_record[1]
                valid, message = mobile_validate(request,Mobile_number)
                if not valid:
                    existnumber_count += 1
                else:
                    CustomerInfo.objects.create(customer = customer,
                                                name = customer_record[0],
                                                mobile_number  = customer_record[1])
                    credit_master.current_credits = credit_master.current_credits - len(customer_records)
                    credit_master.save()
            if existnumber_count == 0:
                existnumber_count = "Uploaded Successfully"
                return render(request, 'referral_csv_upload.html', {'form': form,  'customer' : customer, 'existnumber_count': existnumber_count, 'groupnames': groupnames, 'credit_masters_list' : referal_credit_masters_list, 'isms_credit_masters_list': incommingsms_credit_masters_list})
            elif existnumber_count == 1:
                existnumber_count = str(existnumber_count) + 'Exist Number'
                return render(request, 'referral_csv_upload.html', {'form': form,  'customer' : customer, 'existnumber_count': existnumber_count, 'groupnames': groupnames, 'credit_masters_list' : referal_credit_masters_list, 'isms_credit_masters_list': incommingsms_credit_masters_list})
            else:
                existnumber_count = str(existnumber_count) + 'Exist Numbers'
                return render(request, 'referral_csv_upload.html', {'form': form,  'customer' : customer, 'existnumber_count': existnumber_count, 'groupnames': groupnames, 'credit_masters_list' : referal_credit_masters_list, 'isms_credit_masters_list': incommingsms_credit_masters_list})
        else:
            form = ReferralUploadForm()
            return render(request, 'referral_csv_upload.html', {'form': form,'customer' : customer, 'groupnames': groupnames, 'credit_masters_list' : referal_credit_masters_list, 'isms_credit_masters_list': incommingsms_credit_masters_list})
    else:
        form = ReferralUploadForm()
        return render(request, 'referral_csv_upload.html', {'form': form,'customer' : customer, 'groupnames': groupnames, 'credit_masters_list' : referal_credit_masters_list, 'isms_credit_masters_list': incommingsms_credit_masters_list})


def view_report(request):
    customer = common.get_customer_for_client_user(request.user)
    user = request.user
    session_id = user.id
    groupnames = user.groups.filter(user=session_id)
    if not customer:
        return HttpResponseRedirect('/login/')
    referal_credit_masters_list   = credits.referral_credits_count(user)
    incommingsms_credit_masters_list = credits.incomming_credits_count(user)
    customer_infos = CustomerInfo.objects.filter(customer = customer)
    report_data_list = []
    for customer_info in customer_infos:
        referral_list = ReferralInfo.objects.filter(customer_info = customer_info)
        data = { 'customer_id'              : customer_info.id,
                 'customer_name'            : customer_info.name,
                 'customer_mobile_number'   : customer_info.mobile_number,
                 'customer_date'            : customer_info.created_at,
                 'referral_info_count'      : referral_list.count(),
                 'referral_info_list'       : referral_list,}
        report_data_list.append(data)
    if customer_infos:
        cid=customer_info.id
        ref_list_datas = ReferralInfo.objects.filter(customer_info_id = cid)
        ref_count = ref_list_datas.count()
        if 'id' in request.GET:
            passid= request.GET['id']
            ref_list_datas1 = ReferralInfo.objects.filter(customer_info_id = passid)
            return render(request, "report.html", {'report_data_list': report_data_list , 'ref_count' : ref_count, 'report_data_list1': ref_list_datas1,'credit_masters_list' : referal_credit_masters_list, 'isms_credit_masters_list': incommingsms_credit_masters_list,  'customer' : customer, 'groupnames': groupnames })

        return render(request, "report.html", {'report_data_list': report_data_list , 'ref_count' : ref_count, 'credit_masters_list' : referal_credit_masters_list, 'isms_credit_masters_list': incommingsms_credit_masters_list, 'customer' : customer, 'groupnames': groupnames})
    else:
        return render(request, "report.html", {'report_data_list': report_data_list, 'credit_masters_list' : referal_credit_masters_list, 'isms_credit_masters_list': incommingsms_credit_masters_list, 'customer' : customer, 'groupnames': groupnames} )

def export_csv(request):
    customer = common.get_customer_for_client_user(request.user)
    if not customer:
        return HttpResponseRedirect('/login/')
    customer_infos = CustomerInfo.objects.filter(customer = customer)
    response = HttpResponse(mimetype='text/csv')
    response['Content-Disposition'] = 'attachment; filename="Customer_list.csv"'
    writer = csv.writer(response)
    writer.writerow([ 'Name' , 'MobileNumber','Date'])
    for report_data in customer_infos:
        writer.writerow([report_data.name, report_data.mobile_number , report_data.created_at])
    return response

@csrf_exempt
def smslongcode_callback(request):
    if not request.method in ('GET', 'HEAD'):
        response =  HttpResponse()
        response.status_code = 405
        return response

    if request.method == 'HEAD':
        return  HttpResponse(content_type = "text/plain")
    sms_sid                 =   request.GET.get('SmsSid', None)
    sms_from                =   request.GET.get('From', None)
    sms_to                  =   request.GET.get('To', None)
    received_at             =   request.GET.get('Date', None)
    body                    =   request.GET.get('Body', None)
    received_at = received_at.replace("'", "")
    SmsLongNumberCallback.objects.create(
                                                sms_sid                 =   sms_sid,
                                                sms_from                =   sms_from,
                                                sms_to                  =   sms_to,
                                                received_at             =   received_at,
                                                body                    =   body
                                          )
    return HttpResponse()

@receiver(post_save, sender = CustomerInfo)
def send_sms_to_customer(sender, **kwargs):
    print "received"
    customer_info = kwargs['instance']

    config      = Config.objects.get(customer = customer_info.customer)
    sms_text    = config.sms_text_template_customer.replace("<name>", customer_info.name)

    response = sms_util.send_sms(config.sms_account_username,
                                  config.sms_account_password,
                                  config.sms_sender_id,
                                  customer_info.mobile_number,
                                  sms_text)

    CustomerSmsSentLog.objects.create(customer_info  = customer_info,
                                      sms_text        = sms_text,
                                      sms_delivery_content  = response)


@receiver(post_save, sender = SmsLongNumberCallback)
def determine_customer_and_extract_referral(sender, **kwargs):
    sms_long_number_callback = kwargs['instance']

    sms_long_number     =   sms_long_number_callback.sms_to
    sms_body            =   sms_long_number_callback.body
    sms_keyword         =   sms_body.split(' ')[0]
    sms_received_from   =   sms_long_number_callback.sms_from
    # Determine the master request
    matched_customer_info = None
    matched_config = None
    potential_customer_infos = CustomerInfo.objects.filter(mobile_number = sms_received_from)
    for master_reqeust in potential_customer_infos:
        configs = Config.objects.filter(customer = master_reqeust.customer)
        if not configs:
            continue
        if configs[0].sms_keyword == sms_keyword and configs[0].sms_long_number == sms_long_number:
            matched_customer_info = master_reqeust
            matched_config = configs[0]
            break

    # Extract the referral info
    if not matched_customer_info:
        return # Some Junk request

    sms_body = sms_body.replace(", ", ",") # Some customer might reply with "KEYWORD NUM, FNAME LNAME"
    sms_body_parts = sms_body.split(",")
    referral_name = sms_body_parts[1]
    referral_mobile_number = sms_body_parts[0].split(" ")[1]

    # Save the referral info
    referral_info = ReferralInfo.objects.create(
                                                    customer_info   = matched_customer_info,
                                                    name            = referral_name,
                                                    mobile_number   = referral_mobile_number
                                                )

    # Send SMS to referral
    sms_text_template = matched_config.sms_text_template_referral

    sms_text = sms_text_template.replace("<customer>", matched_customer_info.name)\
                                .replace("<referral>", referral_info.name)

    response = sms_util.send_sms(matched_config.sms_account_username,
                                 matched_config.sms_account_password,
                                 matched_config.sms_sender_id,
                                 referral_info.mobile_number,
                                 sms_text)

    referral_info.sms_text = sms_text
    referral_info.sms_delivery_content = response
    referral_info.save()

def mobile_validate(request,mobilenumber,customer):
    Validate_Mobile = CustomerInfo.objects.filter(customer = customer,mobile_number = mobilenumber)
    if Validate_Mobile:
        return False, "The Mobilenumber already exist"
    else:
        return True, "validate"


def add_customer(request):
    c={}
    c.update(csrf(request))
    if request.method == 'POST':
        customer       = common.get_customer_for_client_user(request.user)
        Customer_Name  = request.POST.get('Customer_Name')
        Mobile_Number  = request.POST.get('mobilenumber')
        valid, message = mobile_validate(request,Mobile_Number,customer)
        if not valid:
            messages.error(request, message)
            return HttpResponseRedirect('/referral/reports/')
        else:
            Verify_Credits, message = verify_credits(customer)
            if not Verify_Credits:
                messages.error(request, message)
            else:
                CustomerInfo.objects.create(customer       = customer,
                                            name           = Customer_Name,
                                            mobile_number  = Mobile_Number)
                credit_master                  = ReferralCreditMaster.objects.get(customer = customer)
                current_credits                = credit_master.current_credits
                credit_master.current_credits  = current_credits - 1
                credit_master.save()
                return HttpResponseRedirect('/referral/reports/')
            return HttpResponseRedirect('/referral/reports/')
    else:
        return HttpResponseRedirect('/referral/reports/',c)


def verify_credits(customer):
    credit_masters  = ReferralCreditMaster.objects.filter(customer = customer)
    if not credit_masters:
        return False, "Credit entries not found."
    credit_master =  credit_masters[0]
    if not credit_master.current_credits > 0:
        return False, "Credits are less. Current Credits = %s" % credit_master.current_credits

    if credit_master.from_validity > datetime.date.today():
        return False, "The credits are valid only from %s" %  credit_master.from_validity

    if credit_master.to_validity < datetime.date.today():
        return False, "Validity expired on  %s" %  credit_master.to_validity

    return True, "Valid"



