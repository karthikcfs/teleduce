from django.db import models
from django_extensions.db.fields import UUIDField

from customer.models import Customer
from util.models import Audit, AuditCallback

class Config(Audit):
    customer            = models.ForeignKey(Customer, unique = True)
    call_customer       = models.BooleanField(default=False)
    call_referral       = models.BooleanField(default=False)
    max_retry_customer  = models.PositiveIntegerField(default = 0)
    sms_long_number     = models.CharField(max_length = 15)
    sms_sender_id       = models.CharField(max_length = 50)
    sms_keyword         = models.CharField(max_length = 15)
    sms_account_username = models.CharField(max_length = 50)
    sms_account_password = models.CharField(max_length = 50)
    sms_text_template_customer = models.TextField(help_text="&lt;name&gt; will be replaced by the given name.")
    sms_text_template_referral = models.TextField(help_text="&lt;customer&gt; &lt;referral&gt; will be replaced by the given names.")

    def __unicode__(self):
        return self.customer.name

    class Meta:
        unique_together = ("sms_long_number", 'sms_keyword')

    sms_text_template_referral.allow_tags = True

class CustomerInfo(AuditCallback): # Customer's customer
    customer       = models.ForeignKey(Customer) # Teleduce Customer
    request_id     = UUIDField()
    name           = models.CharField(max_length = 100) # Customer's customer name
    mobile_number  = models.CharField(max_length = 15)

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        print self
        super(CustomerInfo, self).save(*args, **kwargs) # Call the "real" save() method.


class CustomerSmsSentLog(AuditCallback):
    customer_info  =   models.ForeignKey(CustomerInfo)
    sms_text        = models.TextField()
    sms_delivery_content    = models.TextField(null = True, blank = True)

    def __unicode__(self):
        return self.customer_info.name


class ReferralInfo(AuditCallback):
    customer_info          = models.ForeignKey(CustomerInfo)
    name                    = models.CharField(max_length = 15)
    mobile_number           = models.CharField(max_length = 15)
    sms_text                = models.TextField(null = True, blank = True)
    sms_delivery_content    = models.TextField(null = True, blank = True)

    def __unicode__(self):
        return self.name

class SmsLongNumberCallback(AuditCallback):
    sms_sid                 =   models.CharField(max_length = 50, verbose_name = 'SMS SID')
    sms_from                =   models.CharField(max_length = 13, verbose_name = 'SMS From')
    sms_to                  =   models.CharField(max_length = 13, verbose_name = 'SMS To')
    received_at             =   models.DateTimeField(verbose_name = 'SMS Received At')
    body                    =   models.CharField(max_length = 255, verbose_name = 'SMS Content')

    def __unicode__(self):
        return self.sms_from

    class Meta:
        verbose_name        = "SMS Long Number -> Callback"
        verbose_name_plural = "SMS Long Number -> Callbacks"

class ReferralCreditMaster(Audit):
    customer        = models.ForeignKey(Customer)
    current_credits = models.IntegerField(default=0)
    from_validity   = models.DateField()
    to_validity     = models.DateField()

    def __unicode__(self):
        return self.customer.name
