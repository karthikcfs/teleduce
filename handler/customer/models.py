from django.contrib.auth.models import User
from django.db import models

from util.constants import TELEDUCE_APP_CHOICES
from util.models import Audit


class Customer(Audit):
    customer_id     = models.CharField(max_length = 6, unique = True, verbose_name = 'Customer ID')
    name            = models.CharField(max_length = 50, verbose_name = 'Customer Name')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name        = "Customer Profile"
        verbose_name_plural = "Customer Profiles"

class CustomerTeleduceApp(Audit):
    customer    =   models.ForeignKey(Customer)
    app_name    =   models.CharField(max_length=100, choices = TELEDUCE_APP_CHOICES)

    def __unicode__(self):
        return "%s[%s]" % (self.customer.name, self.app_name)


class CustomerUser(Audit):
    customer = models.OneToOneField(Customer)
    users    = models.ManyToManyField(User, related_name='user_%(class)s_set', verbose_name='Customer User Name', )

    def __unicode__(self):
        return self.customer.name

    def get_assigned_users(self):
        user_list = []
        for user in self.users.all():
            user_list.append(user.username)
        return ", ".join(user_list)
    get_assigned_users.short_description = 'Assigned Users'


    class Meta:
        verbose_name        = "Customer <-> User Assignment"
        verbose_name_plural = "Customer <-> User Assignments"

class CustomerNumber(Audit):
    customer   = models.ForeignKey(Customer)
    number     = models.CharField(max_length = 15, unique = True, verbose_name='Inbound Number',)

    def __unicode__(self):
        return self.customer.name + ' <-> ' + self.number

    class Meta:
        verbose_name        = "Customer <-> Inbound Number"
        verbose_name_plural = "Customer <-> Inbound Numbers"
