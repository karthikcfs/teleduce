from django.contrib import admin

from customer.models import Customer, CustomerTeleduceApp, CustomerUser,\
    CustomerNumber
from util.admin import AuditAdmin

class CustomerAdmin(AuditAdmin):
    list_display = ('customer_id', 'name',) + AuditAdmin.list_display

class CustomerTeleduceAppAdmin(AuditAdmin):
    list_display = ('customer', 'app_name',) + AuditAdmin.list_display

class CustomerUserAdmin(AuditAdmin):
    list_display = ('customer', 'get_assigned_users') + AuditAdmin.list_display

class CustomerNumberAdmin(AuditAdmin):
    list_display = ('customer', 'number',) + AuditAdmin.list_display

admin.site.register(Customer, CustomerAdmin)
admin.site.register(CustomerTeleduceApp, CustomerTeleduceAppAdmin)
admin.site.register(CustomerUser, CustomerUserAdmin)
admin.site.register(CustomerNumber, CustomerNumberAdmin)
