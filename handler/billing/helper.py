import datetime

from billing.models import CreditMaster, CreditChangeLog
from util.common import get_customer_for_client_user
from util.constants import UNIT_MINUTES, SERVICE_TYPE_CALL_LOCAL, \
    TELEDUCE_APP_DTMF


def update_billing_dtmf(call_dtmf):
    app_name        = TELEDUCE_APP_DTMF
    service_type    = SERVICE_TYPE_CALL_LOCAL

    customer        = call_dtmf.customer

    duration_in_mins    = 0

    seconds = 0
    try: # End time could be None.
        seconds = int((call_dtmf.end_time - call_dtmf.start_time).total_seconds())
    except:
        pass # Ignore it

    minutes = seconds / 60.0
    if (minutes - int(minutes)) > 0:
        duration_in_mins = int(minutes) + 1
    else:
        duration_in_mins = int(minutes)


    CreditChangeLog.objects.create(customer     = customer,
                                   service_type = service_type,
                                   app_name     = app_name,
                                   credits_used = duration_in_mins,
                                   unit         = UNIT_MINUTES)

    credit_masters = CreditMaster.objects.filter(customer = customer, service_type = service_type)
    credit_master  = credit_masters[0]
    credit_master.current_credits = credit_master.current_credits - duration_in_mins
    credit_master.save()


def update_billing(call_duration, app_name):
    customer            = call_duration.customer
    service_type        = call_duration.service_type

    duration_in_mins    = 0

    seconds = 0
    try: # End time could be None.
        seconds = int((call_duration.end_time - call_duration.start_time).total_seconds())
    except:
        pass # Ignore it

    minutes = seconds / 60.0
    if (minutes - int(minutes)) > 0:
        duration_in_mins = int(minutes) + 1
    else:
        duration_in_mins = int(minutes)


    CreditChangeLog.objects.create(customer     = customer,
                                   service_type = service_type,
                                   app_name     = app_name,
                                   credits_used = duration_in_mins,
                                   unit         = UNIT_MINUTES)

    credit_masters = CreditMaster.objects.filter(customer     = customer, service_type = service_type)
    credit_master  = credit_masters[0]
    credit_master.current_credits = credit_master.current_credits - duration_in_mins
    credit_master.save()


def verify_credits(user, service_type):
    customer = get_customer_for_client_user(user)
    if not customer:
        return False, "Invalid user account. Not associated with any customers."

    credit_masters  = CreditMaster.objects.filter(customer = customer, service_type = service_type)

    # No Billing details available
    if not credit_masters:
        return False, "Credit records are available. Please contact the support team."

    credit_master = credit_masters[0]

    if not credit_master.status == "ACTIVE":
        return False, "Your account status is not active."

    if not credit_master.current_credits > 0:
        return False, "Credits are less. Current Credits = %s" % credit_master.current_credits

    if credit_master.from_validity > datetime.date.today():
        return False, "The credits are valid only from %s" %  credit_master.from_validity

    if credit_master.to_validity < datetime.date.today():
        return False, "Validity expired on  %s" %  credit_master.to_validity

    return True, "Valid" # Its a valid user
