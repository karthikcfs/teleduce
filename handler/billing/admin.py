from django.contrib import admin

from billing.models import CreditMaster, CreditChangeLog,\
    CustomerRate
from util.admin import AuditAdmin

class CreditMasterAdmin(AuditAdmin):
    list_display = ('customer', 'service_type', 'current_credits', 'unit', 'from_validity', 'to_validity', 'status',) + AuditAdmin.list_display

class CreditChangeLogAdmin(AuditAdmin):
    list_display = ('customer', 'service_type', 'app_name', 'credits_used', 'unit',) + AuditAdmin.list_display

class CustomerRateAdmin(AuditAdmin):
    list_display = ('customer', 'service_type', 'cost', 'unit', 'pulse', 'status',) + AuditAdmin.list_display

admin.site.register(CreditMaster, CreditMasterAdmin)
admin.site.register(CreditChangeLog, CreditChangeLogAdmin)
admin.site.register(CustomerRate, CustomerRateAdmin)