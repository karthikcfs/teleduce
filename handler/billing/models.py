from django.db import models

from customer.models import Customer
from util.constants import SERVICE_TYPE_CHOICES, UNIT_CHOICES,\
    TELEDUCE_APP_CHOICES
from util.models import Audit


class CreditMaster(Audit):
    customer        = models.ForeignKey(Customer)
    service_type    = models.CharField(max_length=100, choices = SERVICE_TYPE_CHOICES)
    current_credits = models.DecimalField(decimal_places = 2, max_digits = 10)
    unit            = models.CharField(max_length = 100, choices = UNIT_CHOICES)
    from_validity   = models.DateField()
    to_validity     = models.DateField()
    status          = models.CharField(max_length=100, choices=(("ACTIVE", "ACTIVE"),
                                                                ("INACTIVE", "INACTIVE")))

    def __unicode__(self):
        return "%s[%s]" % (self.customer, self.service_type)

class CreditChangeLog(Audit):
    customer        = models.ForeignKey(Customer)
    service_type    = models.CharField(max_length=100, choices = SERVICE_TYPE_CHOICES)
    app_name        = models.CharField(max_length=100, choices = TELEDUCE_APP_CHOICES)
    credits_used    = models.DecimalField(decimal_places = 2, max_digits = 10)
    unit            = models.CharField(max_length = 100, choices = UNIT_CHOICES)

    def __unicode__(self):
        return "%s[%s]" % (self.customer, self.service_type)

class CustomerRate(Audit):
    customer        = models.ForeignKey(Customer)
    service_type    = models.CharField(max_length=100, choices = SERVICE_TYPE_CHOICES)
    cost            = models.DecimalField(decimal_places = 2, max_digits = 10)
    unit            = models.CharField(max_length = 100, choices = UNIT_CHOICES)
    status          = models.CharField(max_length=100, choices=(("ACTIVE", "ACTIVE"),
                                                                ("INACTIVE", "INACTIVE")))
    pulse           = models.CharField(max_length=100, choices=(("15S", "15 Seconds"),
                                                                ("30S", "30 Seconds"),
                                                                ("60S", "60 Seconds")))


    def __unicode__(self):
        return "%s[%s]" % (self.customer, self.service_type)
