from django.shortcuts import render
from django.contrib.auth.decorators import user_passes_test
from django.http.response import HttpResponse, HttpResponseRedirect
from login.views import login_App
from util import common, credits, missedcall_area_find_util 
from django.shortcuts import redirect
from django.shortcuts import render_to_response
from datetime import date,timedelta
import datetime
from missed_call.models import MissedCallLog
from long_code.models import SmsLongNumberInboundMessage
from referral.models import CustomerInfo,ReferralInfo
from api.models import CallDuration
# Create your views here.


#Access Application depends on permission
def user_Permissions(request):
    user                             = request.user
    customer                         = common.get_customer_for_client_user(request.user)
    referal_credit_masters_list      = credits.referral_credits_count(user)
    incommingsms_credit_masters_list = credits.incomming_credits_count(user)
    today_min                        = datetime.datetime.combine(date.today(), datetime.time.min)
    today_max                        = datetime.datetime.combine(date.today(), datetime.time.max)
    state_name                       = ""
    today_missedcall_count = total_missedcall_count = today_incommingsms_count = total_incommingsms_count = zero_refered_list_count = one_refered_list_count = multi_refered_list_count = Refered_list_count = percentage = referral_customer_infos_count = referrel_customer_infos = call_count = api_missedcall_count = total_api_missedcall_count = peaktime = no_of_call = redirect_peaktime = redirect_no_of_call  =  missedcall_peaktime = missedcall_peaktime_call = 0
    session_id = user.id
    groupnames = user.groups.filter(user=session_id)
    for groupname in groupnames:
        if groupname.name == 'REFERRAL':
            referral_customer_infos_count    = CustomerInfo.objects.filter(customer = customer).count()
            referrel_customer_infos          = CustomerInfo.objects.filter(customer = customer)
            for referrel_customer_info in referrel_customer_infos:
                refered_list_datas = ReferralInfo.objects.filter(customer_info = referrel_customer_info).count()
                for refered_list_data in str(refered_list_datas):
                    if refered_list_data == "0":
                        zero_refered_list_count +=1
                    elif refered_list_data == "1":
                        one_refered_list_count +=1
                    else:
                        multi_refered_list_count +=1
                    Refered_list_count       = one_refered_list_count + multi_refered_list_count
                    referal_list_percentage  = Refered_list_count * 100
                    percentage               = referal_list_percentage / referral_customer_infos_count
        elif groupname.name == 'MISSEDCALL':
            today_missedcall_count           = MissedCallLog.objects.filter(customer = customer,created_at__range=(today_min, today_max)).count()
            total_missedcall_count           = MissedCallLog.objects.filter(customer = customer).count()
            missedcall_area                  = missedcall_area_find_util.find_area(request, customer)
            state_name                       = find_state_name(request,customer,missedcall_area)
            missedcall_peaktime,missedcall_peaktime_call = find_missedcall_peaktime(request,today_min,today_max,customer)
            if missedcall_peaktime_call == 0:
                missedcall_peaktime = "00:00:00 to 00:00:00"

        elif groupname.name == 'INCOMINGSMS':
            today_incommingsms_count         = SmsLongNumberInboundMessage.objects.filter(customer = customer,created_at__range=(today_min, today_max)).count()
            total_incommingsms_count         = SmsLongNumberInboundMessage.objects.filter(customer = customer).count()
        elif groupname.name == 'CALL LIST':
            today_call_count                             = CallDuration.objects.filter(customer = customer,created_at__range=(today_min, today_max)).count()
            today_redircet_call_count,redirect_count     = credits.get_redirect_callduration_missedcall_count(request,customer,today_min, today_max)
            call_count                       = today_call_count + today_redircet_call_count
            apicallduration_Infos            = CallDuration.objects.filter(customer = customer,created_at__range=(today_min, today_max))
            api_missedcall_count             = 0
            for apicallduration_Info in apicallduration_Infos:
                dial_status_from  = apicallduration_Info.dial_status_from
                dial_status_to    = apicallduration_Info.dial_status_to
                if dial_status_from == "Up" and dial_status_to  == "Up":
                    api_missedcall_count = api_missedcall_count
                else:
                    api_missedcall_count += 1
            total_api_missedcall_count = api_missedcall_count + redirect_count
            peaktime,no_of_call        = find_peaktime_call(request,customer,today_min, today_max)
            redirect_peaktime,redirect_no_of_call = credits.find_peaktime_call(request,customer,today_min, today_max)
    if customer:
        return render(request, "dashboard.html",  {'groupnames'                   : groupnames ,
                                                   'customer'                     : customer,
                                                   'credit_masters_list'          : referal_credit_masters_list,
                                                   'isms_credit_masters_list'     : incommingsms_credit_masters_list,
                                                   'today_missedcall_count'       : today_missedcall_count,
                                                   'total_missedcall_count'       : total_missedcall_count,
                                                   'today_incommingsms_count'     : today_incommingsms_count,
                                                   'total_incommingsms_count'     : total_incommingsms_count,
                                                   'referral_customer_infos_count': referral_customer_infos_count,
                                                   'Refered_list_count'           : Refered_list_count,
                                                   'multi_refered_list_count'     : multi_refered_list_count,
                                                   'percentage'                   : percentage,
                                                   'today_call_count'             : call_count,
                                                   'api_missedcall_count'         : api_missedcall_count,
                                                   'total_api_missedcall_count'   : total_api_missedcall_count,
                                                   'peaktime'                     : redirect_peaktime,
                                                   'no_of_call'                   : redirect_no_of_call,
                                                   "state_name"                   : state_name,
                                                   "missedcall_peaktime"          : missedcall_peaktime,
                                                   "missedcall_peaktime_call"     : missedcall_peaktime_call })
    else:
        return HttpResponseRedirect('/login/')

def find_state_name(request,customer,missedcall_area):
    missedcall_area_count = {}
    if missedcall_area :
        for x in missedcall_area :
            missedcall_area_count[x] = missedcall_area_count.get(x, 0) + 1
            maximumcall_area   = max(missedcall_area_count.iteritems(), key=operator.itemgetter(1))[0]
            starting_letter = ""
            for i in maximumcall_area.upper().split():
                starting_letter += i[0]
                starting_letter += i[1]
                if starting_letter == "AP":
                    state_name = "Andhra Pradesh "
                if starting_letter == "AS":
                    state_name = "Assam"
                if starting_letter == "BR":
                    state_name = "Bihar"
                if starting_letter == "CH":
                    state_name = "Chennai"
                if starting_letter == "DL":
                    state_name = "Delhi"
                if starting_letter == "GJ":
                    state_name = "Gujarat"
                if starting_letter == "HP":
                    state_name = "Himachal Pradesh"
                if starting_letter == "HR":
                    state_name = "Haryana"
                if starting_letter == "JK":
                    state_name = "Jammu and Kashmir"
                if starting_letter == "KL":
                    state_name = "Kerala"
                if starting_letter == "KA":
                    state_name = "Karnataka "
                if starting_letter == "KO":
                    state_name = "Kolkata "
                if starting_letter == "MH":
                    state_name = "Maharashtra "
                if starting_letter == "MP":
                    state_name = "Madhya Pradesh "
                if starting_letter == "MU":
                    state_name = "Mumbai "
                if starting_letter == "NE":
                    state_name = "North East India Telecom Circle "
                if starting_letter == "OR":
                    state_name = "Odisha "
                if starting_letter == "PB":
                    state_name = "Punjab "
                if starting_letter == "RJ":
                    state_name = "Rajasthan "
                if starting_letter == "TN":
                    state_name = "Tamil Nadu "
                if starting_letter == "UE":
                    state_name = "Uttar Pradesh (East) "
                if starting_letter == "UW":
                    state_name = "Uttar Pradesh (West) "
                if starting_letter == "WB":
                    state_name = "West Bengal  "
                if starting_letter == "ZZ":
                    state_name = "Customer Care (All Over India).  "

    else:
        state_name = "NOT RECEIVED"
    return state_name
		
def find_missedcall_peaktime(request,today_min,today_max,customer):
    today_minfrmt    = datetime.datetime.strptime(str(today_min), "%Y-%m-%d %H:%M:%S")
    addhours         = 2
    dt               = datetime.timedelta(hours = addhours)
    two_hour         = today_minfrmt + dt
    four_hour        = two_hour + dt
    six_hour         = four_hour + dt
    eight_hour       = six_hour + dt
    ten_hour         = eight_hour + dt
    twelve_hour      = ten_hour + dt
    fourteen_hour    = twelve_hour + dt
    sixteen_hour     = fourteen_hour + dt
    eighteen_hour    = sixteen_hour + dt
    twenty_hour      = eighteen_hour + dt
    twentytwo_hour   = twenty_hour + dt
    two              = MissedCallLog.objects.filter(customer = customer,created_at__range=(today_min,two_hour)).count()
    four             = MissedCallLog.objects.filter(customer = customer,created_at__range=(two_hour,four_hour)).count()
    six              = MissedCallLog.objects.filter(customer = customer,created_at__range=(four_hour,six_hour)).count()
    eight            = MissedCallLog.objects.filter(customer = customer,created_at__range=(six_hour,eight_hour )).count()
    ten              = MissedCallLog.objects.filter(customer = customer,created_at__range=(eight_hour ,ten_hour)).count()
    twelve           = MissedCallLog.objects.filter(customer = customer,created_at__range=(ten_hour,twelve_hour)).count()
    fourteen         = MissedCallLog.objects.filter(customer = customer,created_at__range=(twelve_hour,fourteen_hour)).count()
    sixteen          = MissedCallLog.objects.filter(customer = customer,created_at__range=(fourteen_hour,sixteen_hour)).count()
    eighteen         = MissedCallLog.objects.filter(customer = customer,created_at__range=(sixteen_hour,eighteen_hour)).count()
    twenty           = MissedCallLog.objects.filter(customer = customer,created_at__range=(eighteen_hour,twenty_hour)).count()
    twentytwo        = MissedCallLog.objects.filter(customer = customer,created_at__range=(twenty_hour,twentytwo_hour)).count()
    twentyfour       = MissedCallLog.objects.filter(customer = customer,created_at__range=(twentytwo_hour,today_max)).count()
    maximum_value    = {'two':two,'four':four,'six':six,'eight':eight,'ten':ten,'twelve':twelve,'fourteen':fourteen,'sixteen':sixteen,'eighteen':eighteen,'twenty':twenty,'twentytwo':twentytwo,'twentyfour':twentyfour}
    no_of_call       = (two,four,six,eight,ten,twelve,fourteen,sixteen,eighteen,twenty,twentytwo,twentyfour)
    max_call         = max(maximum_value.iteritems(), key=operator.itemgetter(1))[0]
    if (max_call == "two"):
        peaktime = "0" + str(today_min.hour) + ":00:00 to    " + "0" + str(two_hour.hour)+ ":00:00"
    elif (max_call == "four"):
        peaktime = "0" + str(two_hour.hour) + ":00:00 to    " + "0" +str(four_hour.hour) + ":00:00"
    elif (max_call == "six"):
        peaktime = "0" + str(four_hour.hour) + ":00:00 to    " + "0" + str(six_hour.hour) + ":00:00"
    elif (max_call == "eight"):
        peaktime = "0" + str(six_hour.hour) + ":00:00 to    " + "0" + str(eight_hour.hour) + ":00:00"
    elif (max_call == "ten"):
        peaktime = "0" + str(eight_hour.hour) + ":00:00 to    " + str(ten_hour.hour) + ":00:00"
    elif (max_call == "twelve"):
        peaktime = str(ten_hour.hour) + ":00:00 to    " + str(twelve_hour.hour) + ":00:00"
    elif (max_call == "fourteen"):
        peaktime = str(twelve_hour.hour) + ":00:00 to    " + str(fourteen_hour.hour) + ":00:00"
    elif (max_call == "sixteen"):
        peaktime = str(fourteen_hour.hour) + ":00:00 to    " + str(sixteen_hour.hour) + ":00:00"
    elif (max_call == "eighteen"):
        peaktime = str(sixteen_hour.hour) + ":00:00 to    " + str(eighteen_hour.hour) + ":00:00"
    elif (max_call == "twenty"):
        peaktime = str(eighteen_hour.hour) + ":00:00 to    " + str(twenty_hour.hour) + ":00:00"
    elif (max_call == "twentytwo"):
        peaktime = str(twenty_hour.hour) + ":00:00 to    " + str(twentytwo_hour.hour) + ":00:00"
    elif (max_call == "twentyfour"):
        peaktime = str(twentytwo_hour.hour) + ":00:00 to    " + str(today_max.hour) + ":00:00"
    return peaktime,max(no_of_call)
		
def find_peaktime_call(request,customer,today_min, today_max):
    today_minfrmt    = datetime.datetime.strptime(str(today_min), "%Y-%m-%d %H:%M:%S")
    addhours         = 2
    dt               = datetime.timedelta(hours = addhours)
    two_hour         = today_minfrmt + dt
    four_hour        = two_hour + dt
    six_hour         = four_hour + dt
    eight_hour       = six_hour + dt
    ten_hour         = eight_hour + dt
    twelve_hour      = ten_hour + dt
    fourteen_hour    = twelve_hour + dt
    sixteen_hour     = fourteen_hour + dt
    eighteen_hour    = sixteen_hour + dt
    twenty_hour      = eighteen_hour + dt
    twentytwo_hour   = twenty_hour + dt
    two              = CallDuration.objects.filter(customer = customer,created_at__range=(today_min,two_hour)).count()
    four             = CallDuration.objects.filter(customer = customer,created_at__range=(two_hour,four_hour)).count()
    six              = CallDuration.objects.filter(customer = customer,created_at__range=(four_hour,six_hour)).count()
    eight            = CallDuration.objects.filter(customer = customer,created_at__range=(six_hour,eight_hour )).count()
    ten              = CallDuration.objects.filter(customer = customer,created_at__range=(eight_hour ,ten_hour)).count()
    twelve           = CallDuration.objects.filter(customer = customer,created_at__range=(ten_hour,twelve_hour)).count()
    fourteen         = CallDuration.objects.filter(customer = customer,created_at__range=(twelve_hour,fourteen_hour)).count()
    sixteen          = CallDuration.objects.filter(customer = customer,created_at__range=(fourteen_hour,sixteen_hour)).count()
    eighteen         = CallDuration.objects.filter(customer = customer,created_at__range=(sixteen_hour,eighteen_hour)).count()
    twenty           = CallDuration.objects.filter(customer = customer,created_at__range=(eighteen_hour,twenty_hour)).count()
    twentytwo        = CallDuration.objects.filter(customer = customer,created_at__range=(twenty_hour,twentytwo_hour)).count()
    twentyfour       = CallDuration.objects.filter(customer = customer,created_at__range=(twentytwo_hour,today_max)).count()
    maximum_value    = {'two':two,'four':four,'six':six,'eight':eight,'ten':ten,'twelve':twelve,'fourteen':fourteen,'sixteen':sixteen,'eighteen':eighteen,'twenty':twenty,'twentytwo':twentytwo,'twentyfour':twentyfour}
    no_of_call       = (two,four,six,eight,ten,twelve,fourteen,sixteen,eighteen,twenty,twentytwo,twentyfour)
    max_call         = max(maximum_value.iteritems(), key=operator.itemgetter(1))[0]
    if (max_call == "two"):
        peaktime = "0" + str(today_min.hour) + ":00:00 to    " + "0" + str(two_hour.hour)+ ":00:00"
#         peaktime = str(today_min) + "to" + str(two_hour)
    elif (max_call == "four"):
        peaktime = "0" + str(two_hour.hour) + ":00:00 to    " + "0" +str(four_hour.hour) + ":00:00"
    elif (max_call == "six"):
        peaktime = "0" + str(four_hour.hour) + ":00:00 to    " + "0" + str(six_hour.hour) + ":00:00"
    elif (max_call == "eight"):
        peaktime = "0" + str(six_hour.hour) + ":00:00 to    " + "0" + str(eight_hour.hour) + ":00:00"
    elif (max_call == "ten"):
        peaktime = "0" + str(eight_hour.hour) + ":00:00 to    " + str(ten_hour.hour) + ":00:00"
    elif (max_call == "twelve"):
        peaktime = str(ten_hour.hour) + ":00:00 to    " + str(twelve_hour.hour) + ":00:00"
    elif (max_call == "fourteen"):
        peaktime = str(twelve_hour.hour) + ":00:00 to    " + str(fourteen_hour.hour) + ":00:00"
    elif (max_call == "sixteen"):
        peaktime = str(fourteen_hour.hour) + ":00:00 to    " + str(sixteen_hour.hour) + ":00:00"
    elif (max_call == "eighteen"):
        peaktime = str(sixteen_hour.hour) + ":00:00 to    " + str(eighteen_hour.hour) + ":00:00"
    elif (max_call == "twenty"):
        peaktime = str(eighteen_hour.hour) + ":00:00 to    " + str(twenty_hour.hour) + ":00:00"
    elif (max_call == "twentytwo"):
        peaktime = str(twenty_hour.hour) + ":00:00 to    " + str(twentytwo_hour.hour) + ":00:00"
    elif (max_call == "twentyfour"):
        peaktime = str(twentytwo_hour.hour) + ":00:00 to    " + str(today_max.hour) + ":00:00"
    return peaktime,max(no_of_call)

def faq(request):
    user = request.user
    customer                         = common.get_customer_for_client_user(request.user)
    referal_credit_masters_list      = credits.referral_credits_count(user)
    incommingsms_credit_masters_list = credits.incomming_credits_count(user)
    session_id                       = user.id
    if customer:
        groupnames = user.groups.filter(user=session_id)
        return render(request, "faq.html",  {'groupnames': groupnames , 'customer': customer,'credit_masters_list' : referal_credit_masters_list, 'isms_credit_masters_list': incommingsms_credit_masters_list })
    else:
        return HttpResponseRedirect('/login/')
def map_view(request):
    return render_to_response("mapview.html")




