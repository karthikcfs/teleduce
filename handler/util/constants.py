MODEL_CHANNEL_EVENTS = (
                            ('Newchannel', 'Newchannel'),
                            ('Newstate', 'Newstate'),
                            ('newexten', 'newexten'),
                            ('NewCallerid', 'NewCallerid'),
                            ('Hangup', 'Hangup'),
                            ('SoftHangupRequest', 'SoftHangupRequest')
                        )

TELEDUCE_APP_CALL = 'CALL'
TELEDUCE_APP_DTMF = 'DTMF'

TELEDUCE_APP_CHOICES = (
                        (TELEDUCE_APP_CALL, TELEDUCE_APP_CALL),
                        (TELEDUCE_APP_DTMF, TELEDUCE_APP_DTMF),
                        )

UNIT_SECONDS  = 'SECONDS'
UNIT_MINUTES  = 'MINUTES'
UNIT_HOURS    = 'HOURS'
UNIT_NUMBERS  = 'NUMBERS'

UNIT_CHOICES = ((UNIT_SECONDS, UNIT_SECONDS),
                (UNIT_MINUTES, UNIT_MINUTES),
                (UNIT_HOURS, UNIT_HOURS),
                (UNIT_NUMBERS, UNIT_NUMBERS))

SERVICE_TYPE_CALL_LOCAL         =   'CALL LOCAL'
SERVICE_TYPE_CALL_STD           =   'CALL STD'
SERVICE_TYPE_CALL_INTER_CIRCLE  =   'CALL INTER CIRCLE'
SERVICE_TYPE_CALL_INTERNATIONAL =   'CALL INTERNATIONAL'
SERVICE_TYPE_SMS_NATIONAL       =   'SMS NATIONAL'
SERVICE_TYPE_SMS_INTERNATIONAL  =   'SMS INTERNATIONAL'

SERVICE_TYPE_CHOICES = ((SERVICE_TYPE_CALL_LOCAL, SERVICE_TYPE_CALL_LOCAL),
                        (SERVICE_TYPE_CALL_STD, SERVICE_TYPE_CALL_STD),
                        (SERVICE_TYPE_CALL_INTER_CIRCLE, SERVICE_TYPE_CALL_INTER_CIRCLE),
                        (SERVICE_TYPE_CALL_INTERNATIONAL, SERVICE_TYPE_CALL_INTERNATIONAL),
                        (SERVICE_TYPE_SMS_NATIONAL, SERVICE_TYPE_SMS_NATIONAL),
                        (SERVICE_TYPE_SMS_INTERNATIONAL, SERVICE_TYPE_SMS_INTERNATIONAL))
