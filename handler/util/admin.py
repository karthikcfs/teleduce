from django.contrib import admin


class BaseAuditAdminCallback(admin.ModelAdmin):
    list_display = ('created_at',)
    save_on_top = True

class AuditAdminCallback(BaseAuditAdminCallback):
    pass

class AuditAdminCallbackWithNoLinks(BaseAuditAdminCallback):
    def get_list_display_links(self, request, list_display):
        """
        Return a sequence containing the fields to be displayed as links
        on the changelist. The list_display parameter is the list of fields
        returned by get_list_display().
        """
        if self.list_display_links or not list_display:
            return self.list_display_links
        else:
            # Use only the first item in list_display as link
            return []
            #return list(list_display)[:1]

def save_model(self, request, obj, form, change):
    instance = form.save(commit=False)
    if not instance.created_at and not instance.modified_at:
        instance.created_by = request.user
    instance.modified_by = request.user
    instance.save()
    form.save_m2m()
    return instance


class BaseAuditAdmin(admin.ModelAdmin):
    list_display = ('created_by', 'modified_by', 'created_at', 'modified_at',)
    exclude = ('created_by', 'modified_by',)
    save_on_top = True

    def save_model(self, request, obj, form, change):
        save_model(self, request, obj, form, change)

class AuditAdmin(BaseAuditAdmin, admin.ModelAdmin):
    pass

