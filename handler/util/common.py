from django.conf import settings
from django.db.models import Q

from customer.models import CustomerUser, CustomerNumber

def getGroupListByUser(user):
    group_list = []
    user_group_list = user.groups.all()
    for group in user_group_list:
        group_list.append(group.name)
    return group_list

def isClientUser(user):
    group_list = getGroupListByUser(user)
    if settings.GROUP_CLIENT_USER in group_list:
        return True
    return False

def get_user_group_based_queryset_for_dtmf_webhook_logs(self, request):
    qs = self.model._default_manager.get_query_set()
    ordering = self.ordering or ()
    if ordering:
        qs = qs.order_by(*ordering)

    # If it is a super user or belongs to site admin, display all.
    if request.user.is_superuser:
        return qs

    group_list = getGroupListByUser(request.user)
    if (settings.GROUP_CLIENT_USER in group_list):
        customer = get_customer_for_client_user(request.user)
        return qs.exclude(~Q(call_dtmf__customer__exact = customer))
    return qs.exclude(~Q(from_extension__exact=0))

def get_user_group_based_queryset_for_missed_call_logs(self, request):
    qs = self.model._default_manager.get_query_set()
    ordering = self.ordering or ()
    if ordering:
        qs = qs.order_by(*ordering)

    # If it is a super user or belongs to site admin, display all.
    if request.user.is_superuser:
        return qs

    group_list = getGroupListByUser(request.user)
    if (settings.GROUP_CLIENT_USER in group_list):
        customer = get_customer_for_client_user(request.user)
        return qs.exclude(~Q(customer__exact = customer))
    return qs.exclude(~Q(customer__exact=0)) # No records

def get_user_group_based_queryset_for_call_duration(self, request):  # CustomerSMSLongNumberKeyword
    qs = self.model._default_manager.get_query_set()
    ordering = self.ordering or ()
    if ordering:
        qs = qs.order_by(*ordering)

    # If it is a super user or belongs to site admin, display all.
    if request.user.is_superuser:
        return qs

    group_list = getGroupListByUser(request.user)
    if (settings.GROUP_CLIENT_USER in group_list):
        customer = get_customer_for_client_user(request.user)
        return qs.exclude(~Q(customer__exact = customer))
    return qs.exclude(~Q(from_extension__exact=0))

# OLD LOGIC
#         call_durations = get_customer_numbers_for_client_user(request.user)
#         return qs.exclude(~Q(from_extension__in = call_durations))
#     return qs.exclude(~Q(from_extension__exact=0))

def get_customer_numbers_for_client_user(user):
    # Get the Numbers assigned to the customer first
    customer_numbers = []
    customer_number_objects = CustomerNumber.objects.filter(customer = get_customer_for_client_user(user))
    for customer_number_object in customer_number_objects:
        customer_numbers.append(customer_number_object.number)
    return customer_numbers
#     # Get the call duration for this numbers
#     return CallDuration.objects.filter(from_extension__in = customer_numbers)

def get_customer_for_client_user(request_user):
    customer_users = CustomerUser.objects.all()

    for customer_user in customer_users:
        for user in customer_user.users.all():
            if request_user == user:
                return customer_user.customer
    return None
