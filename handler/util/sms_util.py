import requests


def send_sms(sms_account_username,
             sms_account_password,
             sms_sender_id,
             mobile_number,
             sms_text):

    sms_url = "http://trans.corefactors.in/sendsms.jsp"

    payload = {'user'       : sms_account_username,
               'password'   : sms_account_password,
               'version'    : '3',
               'mobiles'    : mobile_number,
               'sms'        : sms_text,
               'senderid'   : sms_sender_id
              }

    try:
        r = requests.get(sms_url, params = payload)
        print r.url
        print r
        print type(r)

        sms_delivery_content = ""
        sms_delivery_content += "URL : " + r.url
        sms_delivery_content += " Status Code : " + str(r.status_code)
        sms_delivery_content += " Response Content : " + str(r.content)
        return sms_delivery_content
    except Exception as e:
        return str(e)

    # 103.16.101.52:8080/bulksms/bulksms?username=hry-cfshr&password=cfshr&type=0&dlr=1&destination=8884088830&source=CORFCT&message=test
    #sms_url = "http://103.16.101.52:8080/bulksms/bulksms"
    #payload = {'username'   : sms_account_username,
    #           'password'   : sms_account_password,
    #           'type'       : '0',
    #           'dlr'        : '1',
    #           'destination' : mobile_number,
    #           'message'    : sms_text,
    #           'source'     : sms_sender_id
    #          }
