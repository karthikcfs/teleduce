from long_code.models import SmsLongNumberCreditMaster
from redirect.models import CallDuration
from referral.models import ReferralCreditMaster
from util.common import get_customer_for_client_user


def incomming_credits_count(request):
    customer = get_customer_for_client_user(request)
    Isms_credit_masters  = SmsLongNumberCreditMaster.objects.filter(customer = customer)
    if Isms_credit_masters:
        Isms_credit_masters_list = []
        for customer_credit_info in Isms_credit_masters:
            datas = { 'current_credits'  : customer_credit_info.current_credits,
                      'from_validity'    : customer_credit_info.from_validity,
                      'to_validity'      : customer_credit_info.to_validity,}
            Isms_credit_masters_list.append(datas)
            return Isms_credit_masters_list

def referral_credits_count(request):
    customer = get_customer_for_client_user(request)
    Referral_credit_masters  = ReferralCreditMaster.objects.filter(customer = customer)
    if Referral_credit_masters:
        credit_masters_list = []
        for referal_credit_info in Referral_credit_masters:
            datas = { 'current_credits'  : referal_credit_info.current_credits,
                      'from_validity'    : referal_credit_info.from_validity,
                      'to_validity'      : referal_credit_info.to_validity,}
            credit_masters_list.append(datas)
            return credit_masters_list

def callduration_list(request,customer):
    redirctcallduration_Infos = CallDuration.objects.filter(customer = customer)
    redirctcallduration_Lists = []
    for redirctcallduration_Info in redirctcallduration_Infos:
        start_time           = redirctcallduration_Info.start_time
        end_time             = redirctcallduration_Info.end_time
        if start_time == None:
            difftimes       = 0
            diff_seconds    = 0
            diff_minutes    = 0
            remainder       = 0
        elif end_time == None:
            difftimes       = 0
            diff_seconds    = 0
            diff_minutes    = 0
            remainder       = 0
        else:
            difftimes            = end_time - start_time
            diff_seconds         = int (difftimes.total_seconds())
            diff_minutes         = diff_seconds/60
            remainder            = diff_seconds % 10
        if(remainder == 0):
            diff_minutes = diff_minutes
        else:
            diff_minutes = int(diff_minutes + 1)
        redirctcallduration_data = {'redirect_cid'                  : redirctcallduration_Info.customer_id,
                                    'redirect_customer'             : customer,
                                    'redirect_service_type'         : redirctcallduration_Info.service_type,
                                    'redirect_from_number'          : redirctcallduration_Info.from_number,
                                    'redirect_from_channel'         : redirctcallduration_Info.from_channel,
                                    'redirect_starttime'            : redirctcallduration_Info.start_time,
                                    'redirect_endtime'              : redirctcallduration_Info.end_time,
                                    'redirect_seconds'              : diff_seconds,
                                    'redirect_minutes'              : diff_minutes,
                                    'redirect_calltype'              : redirctcallduration_Info.call_type,
                                    }
        redirctcallduration_Lists.append(redirctcallduration_data)
        return redirctcallduration_Lists

def get_redirct_callduraion_data(request,customer):
    redirctcallduration_Infos = CallDuration.objects.filter(customer = customer)
    return redirctcallduration_Infos
def get_redirect_callduration_missedcall_count(request,customer,today_min, today_max):
    redircet_call_count           = CallDuration.objects.filter(customer = customer,created_at__range=(today_min, today_max)).count()
    redirctcallduration_Infos     = CallDuration.objects.filter(customer = customer,created_at__range=(today_min, today_max))
    redirct_missedcall_count      = 0
    for redirctcallduration_Info in redirctcallduration_Infos:

        call_type  = redirctcallduration_Info.call_type
        if call_type == "ANSWER" :
            redirct_missedcall_count = redirct_missedcall_count
        else:
            redirct_missedcall_count += 1

    return redircet_call_count,redirct_missedcall_count

def find_peaktime_call(request,customer,today_min, today_max):
    today_minfrmt    = datetime.datetime.strptime(str(today_min), "%Y-%m-%d %H:%M:%S")
    addhours         = 2
    dt               = datetime.timedelta(hours = addhours)
    two_hour         = today_minfrmt + dt
    four_hour        = two_hour + dt
    six_hour         = four_hour + dt
    eight_hour       = six_hour + dt
    ten_hour         = eight_hour + dt
    twelve_hour      = ten_hour + dt
    fourteen_hour    = twelve_hour + dt
    sixteen_hour     = fourteen_hour + dt
    eighteen_hour    = sixteen_hour + dt
    twenty_hour      = eighteen_hour + dt
    twentytwo_hour   = twenty_hour + dt
    two              = CallDuration.objects.filter(customer = customer,created_at__range=(today_min,two_hour)).count()
    four             = CallDuration.objects.filter(customer = customer,created_at__range=(two_hour,four_hour)).count()
    six              = CallDuration.objects.filter(customer = customer,created_at__range=(four_hour,six_hour)).count()
    eight            = CallDuration.objects.filter(customer = customer,created_at__range=(six_hour,eight_hour )).count()
    ten              = CallDuration.objects.filter(customer = customer,created_at__range=(eight_hour ,ten_hour)).count()
    twelve           = CallDuration.objects.filter(customer = customer,created_at__range=(ten_hour,twelve_hour)).count()
    fourteen         = CallDuration.objects.filter(customer = customer,created_at__range=(twelve_hour,fourteen_hour)).count()
    sixteen          = CallDuration.objects.filter(customer = customer,created_at__range=(fourteen_hour,sixteen_hour)).count()
    eighteen         = CallDuration.objects.filter(customer = customer,created_at__range=(sixteen_hour,eighteen_hour)).count()
    twenty           = CallDuration.objects.filter(customer = customer,created_at__range=(eighteen_hour,twenty_hour)).count()
    twentytwo        = CallDuration.objects.filter(customer = customer,created_at__range=(twenty_hour,twentytwo_hour)).count()
    twentyfour       = CallDuration.objects.filter(customer = customer,created_at__range=(twentytwo_hour,today_max)).count()
    maximum_value    = {'two':two,'four':four,'six':six,'eight':eight,'ten':ten,'twelve':twelve,'fourteen':fourteen,'sixteen':sixteen,'eighteen':eighteen,'twenty':twenty,'twentytwo':twentytwo,'twentyfour':twentyfour}
    no_of_call       = (two,four,six,eight,ten,twelve,fourteen,sixteen,eighteen,twenty,twentytwo,twentyfour)
    max_call         = max(maximum_value.iteritems(), key=operator.itemgetter(1))[0]
    if (max_call == "two"):
        peaktime = "0" + str(today_min.hour) + ":00:00 to    " + "0" + str(two_hour.hour)+ ":00:00"
    elif (max_call == "four"):
        peaktime = "0" + str(two_hour.hour) + ":00:00 to    " + "0" +str(four_hour.hour) + ":00:00"
    elif (max_call == "six"):
        peaktime = "0" + str(four_hour.hour) + ":00:00 to    " + "0" + str(six_hour.hour) + ":00:00"
    elif (max_call == "eight"):
        peaktime = "0" + str(six_hour.hour) + ":00:00 to    " + "0" + str(eight_hour.hour) + ":00:00"
    elif (max_call == "ten"):
        peaktime = "0" + str(eight_hour.hour) + ":00:00 to    " + str(ten_hour.hour) + ":00:00"
    elif (max_call == "twelve"):
        peaktime = str(ten_hour.hour) + ":00:00 to    " + str(twelve_hour.hour) + ":00:00"
    elif (max_call == "fourteen"):
        peaktime = str(twelve_hour.hour) + ":00:00 to    " + str(fourteen_hour.hour) + ":00:00"
    elif (max_call == "sixteen"):
        peaktime = str(fourteen_hour.hour) + ":00:00 to    " + str(sixteen_hour.hour) + ":00:00"
    elif (max_call == "eighteen"):
        peaktime = str(sixteen_hour.hour) + ":00:00 to    " + str(eighteen_hour.hour) + ":00:00"
    elif (max_call == "twenty"):
        peaktime = str(eighteen_hour.hour) + ":00:00 to    " + str(twenty_hour.hour) + ":00:00"
    elif (max_call == "twentytwo"):
        peaktime = str(twenty_hour.hour) + ":00:00 to    " + str(twentytwo_hour.hour) + ":00:00"
    elif (max_call == "twentyfour"):
        peaktime = str(twentytwo_hour.hour) + ":00:00 to    " + str(today_max.hour) + ":00:00"
    return peaktime,max(no_of_call)

