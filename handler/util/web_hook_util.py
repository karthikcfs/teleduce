import requests


def call_web_hook_url(web_hook_url, params):
    try:
        r = requests.get(web_hook_url, params = params)
        return "URL : {} Status Code : {}. Response Content : {} ".format(r.url, r.status_code, str(r.content))
    except Exception as e:
        return str(e)

