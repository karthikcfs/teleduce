import time
import asterisk.manager
from django.contrib.auth import authenticate,login
from django.contrib.auth.models import User
from django.http.response import HttpResponse

from api.models import CallDuration, CallDtmf, CallDtmfConfig
from billing.helper import verify_credits
from handler.settings import ASTERISK_MANAGER_IP_ADDRESS, \
    ASTERISK_MANAGER_USERNAME, ASTERISK_MANAGER_PASSWORD
from util.common import get_customer_for_client_user
from util.constants import SERVICE_TYPE_CALL_LOCAL


def connect_asterisk():
    manager = asterisk.manager.Manager()
    try:
        print "Attempting to connect " + ASTERISK_MANAGER_IP_ADDRESS
        manager.connect(ASTERISK_MANAGER_IP_ADDRESS)
        manager.login(ASTERISK_MANAGER_USERNAME, ASTERISK_MANAGER_PASSWORD)
        print "connected to " + ASTERISK_MANAGER_IP_ADDRESS

    except asterisk.manager.ManagerSocketException, (errno, reason):
        print "Error connecting to the manager: %s - %s" % (errno, reason)
        return None
    except asterisk.manager.ManagerAuthException, reason:
        print "Error logging in to the manager: %s" % reason
        return None
    except asterisk.manager.ManagerException, reason:
        print "Error: %s" % reason
        return None
    return manager

def perform_authenticate(username, password):

    user = authenticate(username=username, password=password)
    if user is None:
        # the authentication system was unable to verify the username and password
        return False, "The username and password were incorrect."
    # the password verified for the user
    if not user.is_active:


        return False, "The password is valid, but the account has been disabled!"

    return True, "validated"

def perform_authenticate_login(request, username, password):

    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return True, "validated"

        else:
            return False, "The password is valid, but the account has been disabled!"
    else:
        return False, "The username and password were incorrect."



def call_and_receive_dtmf_webhook(request):
    print request.GET
    return HttpResponse("Success")

def call_and_receive_dtmf(request, username, password, to_number, caller_id, webhook_url):
    authenticated, message = perform_authenticate(username, password)
    if not authenticated:
        return HttpResponse("400," + message)

    # Check for credits
    valid, message = verify_credits(User.objects.get(username=username), SERVICE_TYPE_CALL_LOCAL)
    if not valid:
        return HttpResponse("1000," + message)

    manager = connect_asterisk()
    if not manager:
        return HttpResponse("-1,Connectivity issue. Please contact the technical support.")  # todo connection issue

    customer = get_customer_for_client_user(User.objects.get(username=username))
    if not customer:
                return HttpResponse("-1,Account is not associated. Please contact the technical support.")  # todo connection issue
    call_dtmf_configs = CallDtmfConfig.objects.filter(customer = customer)
    if not call_dtmf_configs:
        return HttpResponse("-1,No configuration available. Please contact the technical support.")
    call_dtmf_config = call_dtmf_configs[0]

    # Persist for billing
    call_dtmf = CallDtmf.objects.create(customer = customer, to_number = to_number, webhook_url = webhook_url)
    manager_msg = manager.originate("DAHDI/i1/" + str(to_number), call_dtmf_config.extension, context=call_dtmf_config.context, priority='1')
    response_result = manager_msg.response[0]

    while True: # wait for the hangup code
        time.sleep(1) # time to update the db records
        call_dtmf = CallDtmf.objects.get(pk = call_dtmf.pk)
        if call_dtmf.hang_up_cause: # Call finished
            if 'Error' in response_result: # Error case: Add 1000
                return HttpResponse(str(int(call_dtmf.hang_up_cause) + 1000) + "," +  call_dtmf.request_id)
            else: # Success case. Add 2000
                return HttpResponse(str(int(call_dtmf.hang_up_cause) + 2000) + "," +  call_dtmf.request_id)
        if call_dtmf.dial_status == 'Up': # Call Connected
            return HttpResponse("3000," + call_dtmf.request_id)

    return HttpResponse(response_result + "," + call_dtmf.request_id)


def call_connect(request, username, password, from_number, to_number, caller_id):
    authenticated, message = perform_authenticate(username, password)
    if not authenticated:
        return HttpResponse("400," + message)

    valid, message = verify_credits(User.objects.get(username=username), SERVICE_TYPE_CALL_LOCAL)
    if not valid:
        return HttpResponse("1000," + message)  # todo connection issue

    manager = connect_asterisk()
    if not manager:
        return HttpResponse("-1")  # todo connection issue

    # Persist for billing
    customer = get_customer_for_client_user(User.objects.get(username=username))
    call_duration = CallDuration.objects.create(customer = customer,
                                                service_type = SERVICE_TYPE_CALL_LOCAL,
                                                from_number = from_number,
                                                to_number = to_number)

    manager_msg = manager.originate("DAHDI/i1/" + str(from_number), "67335550", context='siva', priority='1', caller_id=str(caller_id), variables={'OUTBOUNDNUMBER': str(to_number)})
    response_result = manager_msg.response[0]

    while True: # wait for the hangup code
        time.sleep(1) # time to update the db records
        call_duration = CallDuration.objects.get(pk = call_duration.pk)
        if call_duration.hang_up_cause: # Call finished
            if 'Error' in response_result: # Error case: Add 1000
                return HttpResponse(str(int(call_duration.hang_up_cause) + 1000))
            else: # Success case. Add 2000
                return HttpResponse(str(int(call_duration.hang_up_cause) + 2000))
        if call_duration.dial_status_to == 'Up': # Call Connected
            return HttpResponse("3000")
