from django.contrib import admin

from api.models import CallDuration, CallDtmf, CallDtmfWebhookLog, \
    CallDtmfConfig
from util import common
from util.admin import AuditAdminCallback, AuditAdmin


class CallDurationAdmin(AuditAdminCallback):

    def queryset(self, request):
        if request.user.is_superuser:
            self.list_display = ('customer', 'service_type', 'from_number', 'to_number', 'from_channel',
                'to_channel', 'unique_id1', 'unique_id2', 'start_time', 'end_time',
                'dial_status_from', 'dial_status_to', 'seconds', 'minutes',
                'hang_up_cause',) + AuditAdminCallback.list_display
            self.exclude = ()
        elif common.isClientUser(request.user):
            self.list_display = ('from_number', 'to_number', 'start_time', 'end_time', 'minutes') + AuditAdminCallback.list_display
            self.readonly_fields = self.list_display
            self.exclude = ('customer', 'service_type', 'from_channel', 'to_channel', 'unique_id1', 'unique_id2',
                            'dial_status_from', 'dial_status_to', 'seconds', 'hang_up_cause',)
        else:
            raise Exception()
        return common.get_user_group_based_queryset_for_call_duration(self, request)


class CallDtmfConfigAdmin(AuditAdmin):
    list_display = ('customer', 'context', 'extension',) + AuditAdmin.list_display

class CallDtmfAdmin(AuditAdminCallback):
    list_display = ('customer', 'to_number', 'webhook_url', 'request_id', 'dtmf', 'channel', 'unique_id',
                    'dial_status', 'start_time', 'end_time', 'hang_up_cause',) + AuditAdminCallback.list_display

class CallDtmfWebhookLogAdmin(AuditAdminCallback):
    list_display = ('call_dtmf', 'request_url', 'status_code', 'response_content',) + AuditAdminCallback.list_display

    def queryset(self, request):
        if request.user.is_superuser:
            pass
        elif common.isClientUser(request.user):
            self.readonly_fields = self.list_display
        else:
            raise Exception()
        return common.get_user_group_based_queryset_for_dtmf_webhook_logs(self, request)

admin.site.register(CallDuration, CallDurationAdmin)
admin.site.register(CallDtmf, CallDtmfAdmin)
admin.site.register(CallDtmfWebhookLog, CallDtmfWebhookLogAdmin)
admin.site.register(CallDtmfConfig, CallDtmfConfigAdmin)
