from datetime import datetime

from django.db.models import Q

from api import utils
from api.models import CallDuration, CallDtmf, CallDtmfWebhookLog
from listener.base import AsteriskListener
from billing.helper import update_billing, update_billing_dtmf
from util.constants import TELEDUCE_APP_CALL


class DtmfListener(AsteriskListener):

    def __init__(self, event, manager):
        if event.name == 'Newchannel' and event.headers['ChannelStateDesc'] == "Rsrvd":
            channel    =   event.headers['Channel']
            unique_id      =   event.headers['Uniqueid']
            to_number     =   event.headers['Channel'].split('/')[2].split('-')[0]

            call_dtmf = CallDtmf.objects.filter(to_number = to_number, unique_id = None).last()
            if call_dtmf:
                call_dtmf.channel = channel
                call_dtmf.unique_id = unique_id
                call_dtmf.save()

        # Update the channel state
        if event.name == 'Newstate':
            unique_id             =   event.headers['Uniqueid']
            channel_state_desc    =   event.headers['ChannelStateDesc']

            call_dtmfs = CallDtmf.objects.filter(unique_id = unique_id)
            if call_dtmfs: # New channel is for from number
                call_dtmf = call_dtmfs[0]
                call_dtmf.dial_status = channel_state_desc
                call_dtmf.save()
                # channel is up. We need to start the billing from here.
                if channel_state_desc == 'Up':
                    call_dtmf.start_time = datetime.now()
                    call_dtmf.save()

        if event.name == 'Hangup':
            unique_id   =   event.headers['Uniqueid']
            cause       =   event.headers['Cause']

            call_dtmfs = CallDtmf.objects.filter(unique_id = unique_id)
            if call_dtmfs:
                call_dtmf               = call_dtmfs[0]
                call_dtmf.end_time      = datetime.now()
                call_dtmf.hang_up_cause = cause
                call_dtmf.save()

                if call_dtmf.end_time and call_dtmf.start_time: # If both start time and end time available, then only it will be deducted
                    update_billing_dtmf(call_dtmf)

        # {'Value': '56', 'Variable': 'digito', 'Uniqueid': '1391436459.753', 'Privilege': 'dialplan,all', 'Event': 'VarSet', 'Channel': 'DAHDI/i1/8884088830-2f2'}
        if event.name == 'VarSet' and event.headers['Variable'] == 'digito':
            unique_id   =   event.headers['Uniqueid']
            value       =   event.headers['Value']

            call_dtmfs = CallDtmf.objects.filter(unique_id = unique_id)
            if call_dtmfs:
                call_dtmf = call_dtmfs[0]
                call_dtmf.dtmf = value
                call_dtmf.save()

                # Send the response
                log = CallDtmfWebhookLog.objects.create(call_dtmf=call_dtmf)
                utils.send_webhook_response(call_dtmf, log)


class ApiListener(AsteriskListener):

    def __init__(self, event, manager):
        if event.name == 'Newchannel' and event.headers['ChannelStateDesc'] == "Rsrvd":
            for_channel    =   event.headers['Channel']
            unique_id      =   event.headers['Uniqueid']
            for_number     =   event.headers['Channel'].split('/')[2].split('-')[0]

            call_duration = CallDuration.objects.filter(from_number = for_number, unique_id1 = None).last()
            if call_duration: # New channel is for from number
                call_duration.from_channel = for_channel
                call_duration.unique_id1 = unique_id
                call_duration.save()
            else: # It should be for to number. It means first number picked up and second number dial is in progress
                call_duration = CallDuration.objects.filter(to_number = for_number, unique_id2 = None).filter(~Q(unique_id1 = None)).last()
                if call_duration:
                    call_duration.to_channel = for_channel
                    call_duration.unique_id2 = unique_id
                    call_duration.save()

        # Update the channel state for both the numbers
        if event.name == 'Newstate':
            unique_id             =   event.headers['Uniqueid']
            channel_state_desc    =   event.headers['ChannelStateDesc']

            call_duration = CallDuration.objects.filter(unique_id1 = unique_id)
            if len(call_duration) > 0: # New channel is for from number
                call_duration = call_duration[0]
                call_duration.dial_status_from = channel_state_desc
                call_duration.save()
                # First channel is up. We need to start the billing from here.
                if channel_state_desc == 'Up':
                    call_duration.start_time = datetime.now()
                    call_duration.save()
            else: # It should be for to number. It means first number picked up and second number dial is in progress
                call_durations = CallDuration.objects.filter(unique_id2 = unique_id)
                if call_durations:
                    call_duration = call_durations[0]
                    call_duration.dial_status_to = channel_state_desc
                    call_duration.save()

        if event.name == 'Hangup':
            unique_id   =   event.headers['Uniqueid']
            cause       =   event.headers['Cause']

            call_duration = CallDuration.objects.filter(unique_id2 = unique_id)
            if len(call_duration) > 0:
                call_duration = call_duration[0]
                call_duration.end_time = datetime.now()
                call_duration.hang_up_cause = cause
                call_duration.save()
                update_billing(call_duration, TELEDUCE_APP_CALL)
            else:
                call_duration = CallDuration.objects.filter(unique_id1 = unique_id)
                if len(call_duration) > 0:
                    call_duration = call_duration[0]
                    call_duration.end_time = datetime.now()
                    call_duration.hang_up_cause = cause
                    call_duration.save()
#                     update_billing(call_duration, TELEDUCE_APP_CALL) # duplicate billings
