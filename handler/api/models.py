from django.conf import settings
from django.db import models
from django_extensions.db.fields import UUIDField

from customer.models import Customer
from util.constants import SERVICE_TYPE_CHOICES
from util.models import AuditCallback, Audit


class CallDtmfConfig(Audit):
    customer      = models.ForeignKey(Customer)
    context       = models.CharField(max_length = 50)
    extension     = models.CharField(max_length = 15)

    def __unicode__(self):
        return self.customer.name

class CallDtmf(AuditCallback):
    customer            =   models.ForeignKey(Customer)
    to_number           =   models.CharField(max_length = 15)
    webhook_url         =   models.CharField(max_length = 255)
    request_id          =   UUIDField()
    dtmf                =   models.CharField(max_length = 10, null = True, blank = True)
    channel             =   models.CharField(max_length = 50, null = True, blank = True)
    unique_id           =   models.CharField(max_length = 50, null = True, blank = True)
    dial_status         =   models.CharField(max_length = 50, null = True, blank = True)
    start_time          =   models.DateTimeField(null = True, blank = True)
    end_time            =   models.DateTimeField(null = True, blank = True)
    hang_up_cause       =   models.CharField(max_length = 50, null = True, blank = True)

    def __unicode__(self):
        return  ('%s') % (self.to_number)

class CallDtmfWebhookLog(AuditCallback):
    call_dtmf           =   models.ForeignKey(CallDtmf)
    request_url         =   models.CharField(max_length = 255, null = True, blank = True)
    status_code         =   models.CharField(max_length = 10, null = True, blank = True)
    response_content    =   models.TextField(null = True, blank = True)

    def __unicode__(self):
        return  ('%s') % (self.call_dtmf)


class CallDuration(AuditCallback):
    customer            =   models.ForeignKey(Customer, related_name='customer_%(class)s_api_set', null = True, blank = True)
    service_type        =   models.CharField(max_length=100, choices = SERVICE_TYPE_CHOICES)
    from_number         =   models.CharField(max_length = 15)
    to_number           =   models.CharField(max_length = 15)
    from_channel        =   models.CharField(max_length = 50, null = True, blank = True)
    to_channel          =   models.CharField(max_length = 50, null = True, blank = True)
    unique_id1          =   models.CharField(max_length = 50, null = True, blank = True)
    unique_id2          =   models.CharField(max_length = 50, null = True, blank = True)
    dial_status_from    =   models.CharField(max_length = 50, null = True, blank = True)
    dial_status_to      =   models.CharField(max_length = 50, null = True, blank = True)
    start_time          =   models.DateTimeField(null = True, blank = True)
    end_time            =   models.DateTimeField(null = True, blank = True)
    hang_up_cause       =   models.CharField(max_length = 50, null = True, blank = True)

    def __unicode__(self):
        return  ('%s') % (self.from_number)

    def seconds(self):
        try:
            return int((self.end_time - self.start_time).total_seconds())
        except:
            return "-"

    def minutes(self):
        try:
            seconds = int((self.end_time - self.start_time).total_seconds())
            minutes = seconds / 60.0
            if (minutes - int(minutes)) > 0:
                return int(minutes) + 1
            return int(minutes)
        except:
            return "-"

    def audio_file_player(self):
        """audio player tag for admin"""
        player_string = ""
        try:
            file_url = settings.MEDIA_URL + self.unique_id2.split(".")[0] + "-" + self.from_channel.replace("/", "-") + ".wav"
            player_string = '<ul class="playlist"><li style="width:250px;">\
            <a href="%s">%s</a></li></ul>' % (file_url, self.from_number)
            return player_string
        except:
            pass

    audio_file_player.allow_tags = True
    audio_file_player.short_description = 'Recording'
