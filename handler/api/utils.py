import requests


def send_webhook_response(call_dtmf, log):
    payload = {'dtmf': call_dtmf.dtmf, 'request_id': call_dtmf.request_id}

    try:
        r = requests.get(call_dtmf.webhook_url, params=payload)
        log.request_url         =   r.url
        log.status_code         =   r.status_code
        log.response_content    =   r.content
    except Exception as e:
        log.response_content    = str(e)
    log.save()
