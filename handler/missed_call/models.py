from django.db import models
from django_extensions.db.fields import UUIDField

from customer.models import Customer
from util.models import Audit, AuditCallback


class MissedCallConfig(Audit):
    customer        = models.ForeignKey(Customer)
    inbound_number  = models.CharField(max_length = 15, unique = True)
    virtual_number  = models.CharField(max_length = 15, blank = True, unique = True)
    customer_number = models.CharField(max_length = 15, blank = True, unique = True)
    sms_text        = models.TextField(blank = True)
    sms_sender_id   = models.CharField(max_length = 50, blank = True)
    webhook_url     = models.CharField(max_length = 255, null = True, blank = True)
    sms_account_username = models.CharField(max_length = 50, blank = True)
    sms_account_password = models.CharField(max_length = 50, blank = True)

    def __unicode__(self):
        return self.inbound_number

class MissedCallLog(AuditCallback):
    request_id              = UUIDField()
    customer                = models.ForeignKey(Customer)
    call_from               = models.CharField(max_length = 15)
    inbound_number          = models.CharField(max_length = 15)
    sms_text                = models.TextField()
    sms_sender_id           = models.CharField(max_length = 50)
    sms_delivery_content    = models.TextField(null = True, blank = True)
    webhook_url             = models.CharField(max_length = 255, null = True, blank = True)
    status_code             = models.CharField(max_length = 10, null = True, blank = True)
    response_content        = models.TextField(null = True, blank = True)
    status                  = models.TextField(default='Received')


    def __unicode__(self):
        return self.call_from
