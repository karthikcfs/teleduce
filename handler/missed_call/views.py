from django.shortcuts import render
from util.common import get_customer_for_client_user
from util import sms_util, common,callconnect_util, credits
import csv
from django.http.response import HttpResponse, HttpResponseRedirect
from django.core.context_processors import csrf
from missed_call.models import MissedCallLog,MissedCallConfig
from django.core.mail import send_mail
from long_code.models import SmsLongNumberInboundMessage
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags

# from django.core import mail
# import smtplib



# Create your views here.

def view_missedcall_report(request):
    c={}
    c.update(csrf(request))
    customer   = common.get_customer_for_client_user(request.user)
    user       = request.user
    session_id = user.id
    groupnames = user.groups.filter(user=session_id)
    referal_credit_masters_list      = credits.referral_credits_count(user)
    incommingsms_credit_masters_list = credits.incomming_credits_count(user)
    if not customer:
        return HttpResponseRedirect('/login/')
    Missedcall_Infos = MissedCallLog.objects.filter(customer = customer)
    Missedcall_List = []
    for Missedcall_Info in Missedcall_Infos:
        Missedcall_data = {'missedcall_id'                   : Missedcall_Info.id,
                           'missedcall_from'                 : Missedcall_Info.call_from,
                           'missedcall_created_at'           : Missedcall_Info.created_at,
                           'missedcall_inbound_number'       : Missedcall_Info.inbound_number,
                           'missedcall_sms_text'             : Missedcall_Info.sms_text,
                           'missedcall_sms_sender_id'        : Missedcall_Info.sms_sender_id,
                           'missedcall_sms_delivery_content' : Missedcall_Info.sms_delivery_content,
                           'missedcall_status_code'          : Missedcall_Info.status_code,
                           'missedcall_webhook_url'          : Missedcall_Info.webhook_url,
                           'status'                          : Missedcall_Info.status,
                           }
        Missedcall_List.append(Missedcall_data)
        callconnect_response = ""
    if 'id' in request.GET:
        missedcall_id           = request.GET['id']
        call_connect_data       = MissedCallLog.objects.get(id = missedcall_id)
        secondnumber            = call_connect_data.call_from
        getid                   = call_connect_data.customer_id
        missedcall_config_data  = MissedCallConfig.objects.get(customer_id = getid)
        firstnumber             = missedcall_config_data.customer_number
        fromnumber              = missedcall_config_data.virtual_number
        callconnect_response    = callconnect_util.call_url(request, fromnumber, firstnumber, secondnumber)
        if callconnect_response == "3000":
            status_update       = MissedCallLog.objects.filter(id = missedcall_id).update(status ='Contacted')
        else :
            status_update       = MissedCallLog.objects.filter(id = missedcall_id).update(status ='No Response')
        return HttpResponseRedirect('/missedcall/reports/')

    return render(request, "missedcall_report.html", {'Missedcall_List'          : Missedcall_List ,
                                                      'customer'                 : customer,
                                                      'groupnames'               : groupnames,
                                                      'credit_masters_list'      : referal_credit_masters_list,
                                                      'isms_credit_masters_list' : incommingsms_credit_masters_list,
#                                                       'status'                   : Missedcall_Info.status,
                                                      })


def export_csv(request):
    customer = common.get_customer_for_client_user(request.user)
    if not customer:
        return HttpResponseRedirect('/login/')
    Missedcall_Infos = MissedCallLog.objects.filter(customer = customer)
    response         = HttpResponse(mimetype='text/csv')
    response['Content-Disposition'] = 'attachment; filename="missed_list.csv"'
    writer = csv.writer(response)
    writer.writerow(['From Number' , 'Inbound Number' , 'Received Date', 'SMS status','Web Request Status'])
    for report_data in Missedcall_Infos:
        if not report_data.webhook_url:
            webhook_url = "Not Applicable"
        elif report_data.status_code == "200":
            webhook_url = "sent"
        else:
            webhook_url = "Request Failed"
        writer.writerow([report_data.call_from, report_data.inbound_number , report_data.created_at, 'sent' , webhook_url ])
    return response


def sendmail(request):
    customer = common.get_customer_for_client_user(request.user)
    LongNumber_Infos = SmsLongNumberInboundMessage.objects.filter(customer = customer)
    total_count_records = LongNumber_Infos.count()
    LongNumber_List = []
    for LongNumber_Info in LongNumber_Infos:
        Longnumber_data = {'sms_from'  : LongNumber_Info.sms_from,}
        LongNumber_List.append(Longnumber_data)
    subject, from_email, to = 'Inbound SMS info', 'fletcherram@gmail.com', 'fletcherram@gmail.com'
    html_content = render_to_string('mail.html', {'total_count_records': total_count_records})
    text_content = strip_tags(html_content)
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()
    return HttpResponse("Success")
#     send_mail('Testing', subject, '',
#     ['fletcherram@gmail.com'], fail_silently=False)
#     return HttpResponse("Success")
