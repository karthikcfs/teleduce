from django.contrib import admin

from missed_call.models import MissedCallConfig, MissedCallLog
from util import common
from util.admin import AuditAdmin, AuditAdminCallback


class MissedCallConfigAdmin(AuditAdmin):
    list_display = ('customer', 'inbound_number','virtual_number','customer_number', 'sms_text', 'sms_sender_id', 'webhook_url' ,'sms_account_username', 'sms_account_password') + AuditAdmin.list_display

class MissedCallLogAdmin(AuditAdminCallback):
    list_display = ('customer', 'call_from', 'inbound_number', 'sms_text', 'sms_sender_id', 'sms_delivery_content', 'webhook_url', 'status_code', 'response_content') + AuditAdminCallback.list_display

    def queryset(self, request):
        if request.user.is_superuser:
            self.list_display = ('customer', 'call_from', 'inbound_number', 'sms_text',
                                 'sms_sender_id', 'sms_delivery_content', 'webhook_url',
                                 'status_code', 'response_content') + AuditAdminCallback.list_display
            self.exclude = ()
        elif common.isClientUser(request.user):
            self.list_display = ('call_from', 'inbound_number', 'sms_text',
                                 'sms_sender_id', 'sms_delivery_content', 'webhook_url',
                                 'status_code', 'response_content') + AuditAdminCallback.list_display

            self.readonly_fields = self.list_display
            self.exclude = ('customer', )
        else:
            raise Exception()
        return common.get_user_group_based_queryset_for_missed_call_logs(self, request)

admin.site.register(MissedCallConfig, MissedCallConfigAdmin)
admin.site.register(MissedCallLog, MissedCallLogAdmin)