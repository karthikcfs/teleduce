import requests

from listener.base import AsteriskListener
from missed_call.models import MissedCallConfig, MissedCallLog


def send_sms_and_call_webhook(from_number, current_missed_call_config):
    log = MissedCallLog.objects.create(customer         = current_missed_call_config.customer,
                                       call_from        = from_number,
                                       inbound_number   = current_missed_call_config.inbound_number,
                                       sms_text         = current_missed_call_config.sms_text,
                                       sms_sender_id    = current_missed_call_config.sms_sender_id)

    send_sms(from_number, current_missed_call_config, log)
    call_webhook(from_number, current_missed_call_config, log)

def send_sms(from_number, current_missed_call_config, log):
    sms_url = "http://trans.corefactors.in/sendsms.jsp"
    payload = {'user'       : current_missed_call_config.sms_account_username,
               'password'   : current_missed_call_config.sms_account_password,
               'version'    : '3',
               'mobiles'    : from_number,
               'sms'        : current_missed_call_config.sms_text,
               'senderid'   : current_missed_call_config.sms_sender_id
               }
    try:
        r = requests.get(sms_url, params = payload)
        print r.url
        print r
        print type(r)

        sms_delivery_content = ""
        sms_delivery_content += "URL : " + r.url
        sms_delivery_content += " Status Code : " + str(r.status_code)
        sms_delivery_content += " Response Code : " + str(r.content)
        log.sms_delivery_content = sms_delivery_content
    except Exception as e:
        log.sms_delivery_content    = str(e)
    log.save()

def call_webhook(from_number, current_missed_call_config, log):
    if not current_missed_call_config.webhook_url:
        return

    payload = {'from_number': from_number, 'request_id': log.request_id}

    try:
        r = requests.get(current_missed_call_config.webhook_url, params=payload)
        print r.url
        print r
        print type(r)
        log.webhook_url         =   r.url
        log.status_code         =   r.status_code
        log.response_content    =   r.content
    except Exception as e:
        log.response_content    = str(e)
    log.save()

class MissedCallListener(AsteriskListener):
    def __init__(self, event, manager):
        if event.name == 'Newchannel':
            missed_call_configs = MissedCallConfig.objects.all()
            inbound_number = event.headers['Exten'] # Get the inbound number
            if not inbound_number:
                return # It can be empty

            # Get the configuration for this number
            current_missed_call_config = None
            for missed_call_config in missed_call_configs:
                if inbound_number == missed_call_config.inbound_number:
                    current_missed_call_config = missed_call_config
                    break
            if not current_missed_call_config:
                return # This number is for missed call

            from_number = event.headers['CallerIDNum']

            # Send SMS & Call webhook
            send_sms_and_call_webhook(from_number, current_missed_call_config)
