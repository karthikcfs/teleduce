from __future__ import absolute_import
from django.core.mail import send_mail
from django.http.response import HttpResponse
from celery import Celery
from long_code.models import SmsLongNumberInboundMessage
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
# from util import common
app = Celery('tasks', broker='amqp://guest@localhost//')

@app.task
def mailsend():

#     customer    = common.get_customer_for_client_user()
#     print customer
    LongNumber_Infos = SmsLongNumberInboundMessage.objects.filter(customer_id = 2)
    total_count_records = LongNumber_Infos.count()
    LongNumber_List = []
    for LongNumber_Info in LongNumber_Infos:
        Longnumber_data = {'sms_from'  : LongNumber_Info.sms_from,}
        LongNumber_List.append(Longnumber_data)
    subject, from_email, to = 'Inbound SMS info', 'fletcherram@gmail.com', 'fletcherram@gmail.com'
    html_content = render_to_string('mail.html', {'total_count_records': total_count_records})
    text_content = strip_tags(html_content)
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()
    return user


