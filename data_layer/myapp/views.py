import json

from django.contrib.auth import authenticate
from django.http.response import HttpResponse
from django.shortcuts import render
from django.core import serializers

from myapp.models import Customer
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def customer_create(request):
    if request.method == 'GET':
        username = request.GET['username']
        password = request.GET['password']

        user = authenticate(username=username, password=password)
        if user is None:
        # the authentication system was unable to verify the username and password
            return HttpResponse("The username and password were incorrect.")

        customer_id    = request.GET['customer_id']
        customer_name  = request.GET['customer_name']

        Customer.objects.create(customer_id= customer_id,
                                name = customer_name)
        return HttpResponse("Created Successfully!")
    return HttpResponse("NOT SUPPORTED")



def customer_count(request):
    if request.method == 'GET':
        username = request.GET['username']
        password = request.GET['password']

        user = authenticate(username=username, password=password)
        if user is None:
        # the authentication system was unable to verify the username and password
            return HttpResponse("The username and password were incorrect.")
        return HttpResponse(Customer.objects.count() - 1)
    return HttpResponse("NOT SUPPORTED")


def read_all_customer_list(request):
    if request.method == 'GET':
        username = request.GET['username']
        password = request.GET['password']

        user = authenticate(username=username, password=password)
        if user is None:
        # the authentication system was unable to verify the username and password
            return HttpResponse("The username and password were incorrect.")

        serialized_queryset = serializers.serialize('json', Customer.objects.all())
        return HttpResponse(json.dumps(serialized_queryset), content_type="application/json", mimetype="application/json")

    return HttpResponse("NOT SUPPORTED")
