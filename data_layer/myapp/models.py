from django.db import models

class Customer(models.Model):
    customer_id     = models.CharField(max_length = 6, unique = True, verbose_name = 'Customer ID')
    name            = models.CharField(max_length = 50, verbose_name = 'Customer Name')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name        = "Customer Profile"
        verbose_name_plural = "Customer Profiles"
