from django.conf.urls import patterns, include, url
from django.contrib import admin

from myapp.views import read_all_customer_list, customer_count, customer_create


admin.autodiscover()

urlpatterns = patterns('',
    url(r'^read_all_customer_list/$', read_all_customer_list),
    url(r'^customer_count/$', customer_count),
    url(r'^customer_create/$', customer_create),
    url(r'^admin/', include(admin.site.urls)),

)
