from django.utils.safestring import mark_safe

import json
from pprint import pprint

from django.http.response import HttpResponse
from django.shortcuts import render
import requests


def customer_create(request):
    payload = {'username': 's', 'password': 's', 'customer_id': '5', 'customer_name' : 'EEE'}
    r = requests.get("http://localhost:8001/customer_create/", params=payload)
    print r.content
    return HttpResponse(r.content)

# Create your views here.
def customer_list(request):
    payload = {'username': 's', 'password': 's'}
    r = requests.get("http://localhost:8001/read_all_customer_list/", params=payload)

    content = r.content[1:]
    content = content[:-1]
    json_data = json.loads(content.replace("\\", ""))


    customer_list = []
    for customer_data in json_data:
        customer = customer_data['fields']
        customer_list.append({'id': customer['customer_id'], 'name': customer['name'] })

    r = requests.get("http://localhost:8001/customer_count/", params=payload)

    return render(request, "customer_list.html", {'customers': customer_list, 'customer_count': r.content})

def json_parse(request):
    json_str = '{"employees":[ \
                            {"firstName":"John",    "lastName":"Doe" }, \
                            {"firstName":"Anna",    "lastName":"Smith" }, \
                            {"firstName":"Peter",   "lastName":"Jones" } \
                        ]}'
    return render(request, "json_parse.html", {'json_str': mark_safe(json_str), })
