from django.conf.urls import patterns, include, url
from django.contrib import admin

from myviewapp import views


urlpatterns = patterns('',
    url(r'^customer_list/$', views.customer_list),
    url(r'^create_customer/$', views.customer_create),
    url(r'^json_parse/$', views.json_parse),

)
